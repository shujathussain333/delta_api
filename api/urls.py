
from django.urls import path
from rest_framework.routers import DefaultRouter
from django.conf import settings
from api.views import views
from django.conf.urls.static import static

usesrs_router = DefaultRouter()
# usesrs_router.register(r'users', views.UserViewSet, base_name='user')

urlpatterns = [
    path('roles', views.GetAvailableRoles.as_view(), name='get-available-roles'),
    path('watchlist', views.GetEnabledWatchlists.as_view(), name='get-enabled-watchlists'),
    path('denotified_lists',views.GetDenotifiedWatchlists.as_view(), name='get-denotified-lists'),
    path('subwatch_list_status', views.StatusSubwatchlist.as_view(), name='staus-updated'),
    path('file_download', views.FileDownloadView.as_view(), name='file_downloading'),
    path('fileupload',views.FileUploadView.as_view(),name='file_uploading'),
    path('delta_sanctions_file',views.DeltaSanctionsFileUploadView.as_view(),name='delta_sanctions_file_uploading'),
    path('delta_criminals_file',views.DeltaCriminalsFileUploadView.as_view(),name='delta_criminals_file_uploading'),
    path('uploaded_files',views.DeltaUploadedFiles.as_view(),name='delta_uploaded_files')

    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
