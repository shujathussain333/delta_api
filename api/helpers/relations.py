from api.models import ListMembership, Watchlist


def assign_watchlists(client):
    w_lists = Watchlist.objects.all()

    for wl in w_lists:
        ListMembership.objects.create(
            watchlist=wl,
            client = client,
            viewable = True,
            downloadable = wl.listtype.upper() != 'PEP' 
        )
