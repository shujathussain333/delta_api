from django.shortcuts import get_object_or_404
from rest_framework import serializers

from api.models import Role, User, Watchlist, ReviewEntities, Subwatchlist,File



class FileSerializer(serializers.ModelSerializer):
    class Meta():
        model = File
        fields = ['file']

class RoleNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Role
        fields = ['name']


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    is_active = serializers.BooleanField(default=True)
    role = serializers.IntegerField(source='role.pk')

    class Meta:
        model = User
        fields = ('id', 'email', 'password', 'first_name', 'last_name', 'is_active', 'role')

    def create(self, validated_data):
        email = validated_data.get('email')
        password = validated_data.get('password')
        role_id = validated_data.get('role')['pk']
        role = get_object_or_404(Role, pk=role_id)
        user = User.objects.create_user(email, password=password, role=role)
        user.first_name = validated_data.get('first_name', '')
        user.last_name = validated_data.get('last_name', '')
        user.is_active = validated_data.get('is_active', True)
        user.save()
        return user

    def update(self, user, validated_data):
        user.email = validated_data.get('email', user.email)
        user.first_name = validated_data.get('first_name', user.first_name)
        user.last_name = validated_data.get('last_name', user.last_name)
        user.is_active = validated_data.get('is_active', user.is_active)
        if 'password' in validated_data:
            user.set_password(validated_data.get('password'))
        if 'role' in validated_data:
            role_id = validated_data.get('role')['pk']
            user.role = get_object_or_404(Role, pk=role_id)
        user.save()
        return user


class WatchlistSerializer(serializers.ModelSerializer):

    class Meta:
        model = Watchlist
        fields = '__all__'

class SubwatchlistStatusSerializer(serializers.Serializer):
    status = serializers.CharField()
    sublistid = serializers.IntegerField()


    def validate_status(self, value):
        """
        Check that the blog post is about Django.
        """
        if value not in ['accept','reject']:
            raise serializers.ValidationError("status can be accept or reject")
        return value    


class ReviewEntitiesSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReviewEntities
        fields = '__all__'


class SubwatchlistSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subwatchlist
        fields = ['sublistid', 'version', 'lastmodified']