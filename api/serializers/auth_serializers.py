from rest_framework import serializers
from djoser.serializers import UserSerializer

from api.models import User
from api.serializers.serializers import RoleNameSerializer


class MeSerializer(UserSerializer):
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    role = RoleNameSerializer(read_only=True)
    
    class Meta:
        model = User
        fields = tuple(User.REQUIRED_FIELDS) + (
            User._meta.pk.name,
            User.USERNAME_FIELD,
            'first_name', 'last_name', 'role'
        )
        read_only_fields = (User.USERNAME_FIELD, )