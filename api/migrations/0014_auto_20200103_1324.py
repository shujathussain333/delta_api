# Generated by Django 2.0.7 on 2020-01-03 08:24

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0013_auto_20200103_0659'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subwatchlist',
            name='createddate',
            field=models.DateTimeField(db_column='CreatedDate', default=datetime.datetime(2020, 1, 3, 8, 24, 10, 238972, tzinfo=utc)),
        ),
    ]
