
from rest_framework.permissions import BasePermission

class IsAdministrator(BasePermission):
    message = 'you don\'t have enough permissions.'
    def has_permission(self, request, view):
        if request.user.role.name == "admin".lower():
            return True
        return False

class IsHbluser(BasePermission):
    message = 'you don\'t have enough permissions.'
    def has_permission(self, request, view):
        if request.user.role.name == "hbl_user".lower():
            return True
        return False

