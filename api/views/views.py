from django.conf import settings
import json
from django.db import connections, connection
from rest_framework import generics, viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from django.core.mail import send_mail
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
from api import permissions
from api.serializers import serializers
from rest_condition import Or
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from api.permissions import IsAdministrator,IsHbluser
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from api.models import *
import time
from datetime import datetime
from rest_framework.parsers import MultiPartParser
from decouple import config
import os

class GetAvailableRoles(generics.ListAPIView):
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated,Or(IsAdministrator,IsHbluser)) 
    serializer_class = serializers.RoleNameSerializer
    def get_queryset(self):
        role_id = self.request.user.role.id
        return Role.objects.filter(id=role_id)    

class FileUploadView(APIView):
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated,Or(IsAdministrator,IsHbluser))         
    parser_classes = (MultiPartParser,)

    def post(self, request, format=None):
        data = ''
        file_name = request.FILES['file']
        file_serializer = serializers.FileSerializer(data=request.data)
        if file_serializer.is_valid():
          file_serializer.save()
          data = 'file uploaded successfully'
        else:
           data = 'file not uploaded'   
        data = {"data" : data}
        error = None
        error_code = None    
        return Response({'error': error, 'error_code': error_code, 'data': data},status=200)


class DeltaSanctionsFileUploadView(APIView):
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated,Or(IsAdministrator,IsHbluser))         
    parser_classes = (MultiPartParser,)
    def post(self, request, format=None):
        cursor_hbldelta = connections['default'].cursor()
        data = ''
        file_name = request.FILES['file']
        file_serializer = serializers.FileSerializer(data=request.data)
        if file_serializer.is_valid():
          file_serializer.save()
          data = 'file uploaded successfully'
        else:
           data = 'file not uploaded' 
        
        file_to_send = 'media/uploaded_files/{}'.format(file_name)
        cursor_hbldelta.execute("""INSERT INTO api_uploadedfiles (FileName) VALUES ('{}') """.format(file_to_send))
        link = 'http://142.93.92.13:8030/' + file_to_send            
        data = {"data" : data,"link":link}
        error = None
        error_code = None    
        return Response({'error': error, 'error_code': error_code, 'data': data},status=200)


class DeltaCriminalsFileUploadView(APIView):
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated,Or(IsAdministrator,IsHbluser))         
    parser_classes = (MultiPartParser,)
    def post(self, request, format=None):
        cursor_hbldelta = connections['default'].cursor()
        data = ''
        file_name = request.FILES['file']
        file_serializer = serializers.FileSerializer(data=request.data)
        if file_serializer.is_valid():
          file_serializer.save()
          data = 'file uploaded successfully'
        else:
           data = 'file not uploaded' 
        file_to_send = 'media/uploaded_files/{}'.format(file_name)
        cursor_hbldelta.execute("""INSERT INTO api_uploadedfiles (FileName) VALUES ('{}') """.format(file_to_send))
        link = 'http://142.93.92.13:8030/' + file_to_send            
        data = {"data" : data,"link":link}
        error = None
        error_code = None    
        return Response({'error': error, 'error_code': error_code, 'data': data},status=200) 


class DeltaUploadedFiles(APIView):
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated,Or(IsAdministrator,IsHbluser))     
    def get(self, request):
        cursor_hbldelta = connections['default'].cursor()
        cursor_hbldelta.execute("""select top 2 * from api_uploadedfiles order by FileID desc""")

        records = cursor_hbldelta.fetchall() 
        link1 = 'http://142.93.92.13:8030/' + records[0][1]
        link2 = 'http://142.93.92.13:8030/' + records[1][1]
        file_links = {"Delta_sanction_list":link1,"Delta_criminals_list":link2}
        data = file_links
        error = None
        data = data  
        error_code = None
        return Response({'error': error, 'error_code': error_code, 'data': data},status=200)




class GetEnabledWatchlists(APIView):
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated,Or(IsAdministrator,IsHbluser))     
    def get(self, request):
        cursor_hbldelta = connections['default'].cursor()
        cursor_hbldelta.execute("""SELECT TOP 6 l.description,l.ListID, l.Program, l.ApprovedSubListID, swl.Filename, swl.Status, swl.Version,swl.LastModified,swl.createdDate,l.DrafDeclinedSubListID, swl2.FileName, swl2.Status, swl2.Version,swl2.LastModified, (select top 1 createdDate from tbl_subwatchlist order by SubListID desc) as latestcreatedDate FROM (
                                   SELECT wl.ListID, wl.Program,wl.Description , MAX(CASE WHEN swl.Status = 'approved' THEN swl.SubListID ELSE 0 END) AS ApprovedSubListID, MAX(CASE WHEN swl.Status IN ('declined', 'draft') THEN swl.SubListID ELSE 0 END) AS DrafDeclinedSubListID from tbl_SubWatchList as swl JOIN tbl_WatchList as wl ON (wl.ListID = swl.ListID) GROUP BY wl.ListID, wl.Program,wl.Description
                                   ) as l 
                                   LEFT JOIN tbl_SubWatchList as swl ON (l.ApprovedSubListID = swl.SubListID)
                                   LEFT JOIN tbl_SubWatchList as swl2 ON (l.DrafDeclinedSubListID = swl2.SubListID)""")

        records = cursor_hbldelta.fetchall()
        records = sorted(records, key = lambda x: int(x[1]))

        records = [records[0],records[5],records[1],records[2]]
        data = {"data" : records}
        error = None
        data = data  
        error_code = None
        return Response({'error': error, 'error_code': error_code, 'data': data},status=200) 

class GetDenotifiedWatchlists(APIView):
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated,Or(IsAdministrator,IsHbluser))     
    def get(self, request):
        cursor_hbldelta = connections['default'].cursor()
        cursor_hbldelta.execute("""SELECT TOP 6 l.description,l.ListID, l.Program, l.ApprovedSubListID, swl.Filename, swl.Status, swl.Version,swl.LastModified,swl.createdDate,l.DrafDeclinedSubListID, swl2.FileName, swl2.Status, swl2.Version,swl2.LastModified, (select top 1 createdDate from tbl_subwatchlist order by SubListID desc) as latestcreatedDate FROM (
                                   SELECT wl.ListID, wl.Program,wl.Description , MAX(CASE WHEN swl.Status = 'approved' THEN swl.SubListID ELSE 0 END) AS ApprovedSubListID, MAX(CASE WHEN swl.Status IN ('declined', 'draft') THEN swl.SubListID ELSE 0 END) AS DrafDeclinedSubListID from tbl_SubWatchList as swl JOIN tbl_WatchList as wl ON (wl.ListID = swl.ListID) GROUP BY wl.ListID, wl.Program,wl.Description
                                   ) as l 
                                   LEFT JOIN tbl_SubWatchList as swl ON (l.ApprovedSubListID = swl.SubListID)
                                   LEFT JOIN tbl_SubWatchList as swl2 ON (l.DrafDeclinedSubListID = swl2.SubListID)""")

        records = cursor_hbldelta.fetchall()
        records = sorted(records, key = lambda x: int(x[1]))        
        data = {"data" :records[3:5]}
        error = None
        data = data  
        error_code = None
        return Response({'error': error, 'error_code': error_code, 'data': data},status=200)        

class LogoutView(APIView):
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated,Or(IsAdministrator,IsHbluser)) 
    def get(self, request):   
        data = {"data" : 'user logged out successfully'}
        error = None
        error_code = None    
        return Response({'error': error, 'error_code': error_code, 'data': data},status=200)

class FileDownloadView(APIView):
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated,Or(IsAdministrator,IsHbluser)) 
    def post(self, request):   
        data = {"data" : 'file downloaded successfully'}
        error = None
        error_code = None    
        return Response({'error': error, 'error_code': error_code, 'data': data},status=200)        


class StatusSubwatchlist(APIView):

    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated,Or(IsAdministrator,IsHbluser))    
    def post(self, request):
        timestr = time.strftime("%Y-%m-%d")
        cursor_hbldelta = connections['default'].cursor()      
        status = request.data['status']
        sublistid = request.data['SubListID']
        serializer = serializers.SubwatchlistStatusSerializer(data={'status':request.data['status'],'sublistid':request.data['SubListID']})
        if serializer.is_valid(): 
            data = ''
            if status == 'accept':
                Subwatchlist.objects.filter(sublistid=sublistid).update(status='approved', lastmodified=datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
                subwatchlist = Subwatchlist.objects.get(sublistid=sublistid)
                file_to_send = subwatchlist.filename
                file_to_send = 'http://142.93.92.13:8030' + file_to_send                
                ############email sending/////////////////////////
                msg_body = """\
                <html>
                  <body>
                    <p>Hi Team,</p>
                    <p>The draft of {} has been <b>accepted</b> by the admin and is now visible and available for user.</p>  
                    <p></p>
                    <p>Regards,</p>
                    <p style="color:blue">Support Team</p>                  
                  </body>
                </html>
                """.format(subwatchlist.listid.program)
                message = MIMEMultipart()
                message['From'] = 'support team'
                message['To'] = 'lfdDeltagroup'
                message['Subject'] = 'Data Accepted - DELTA'
                message.attach(MIMEText(msg_body,'html'))

                server = smtplib.SMTP('smtp.gmail.com:587')
                server.starttls()
                server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASS)
                server.sendmail(settings.EMAIL_HOST_USER,
                                    ['lfddelta@googlegroups.com'],
                                    message.as_string())
                server.quit()
                ############email sending/////////////////////////
                cursor_hbldelta.execute("""INSERT INTO tbl_approvedentities (EDateTime,SubListID,ECategory,OrigName,Address,DOB,Nationality,Designation,Organization,Title,DO_Inclusion,DO_Exclusion,Aliases,Matching,ListType,Remarks,Status,Gender,Last_Occupation,Documents,Name)SELECT EDateTime,SubListID,ECategory,OrigName,Address,DOB,Nationality,Designation,Organization,Title,DO_Inclusion,DO_Exclusion,Aliases,Matching,ListType,Remarks,Status,Gender,Last_Occupation,Documents,Name FROM tbl_reviewentities WHERE SubListID = {}  and SubListID is not null; """.format(sublistid)) 
                cursor_hbldelta.execute("""INSERT INTO tbl_historicalentities (EDateTime,SubListID,ECategory,OrigName,Address,DOB,Nationality,Designation,Organization,Title,DO_Inclusion,DO_Exclusion,Aliases,Matching,ListType,Remarks,Status,Gender,Last_Occupation,Documents,Name)SELECT EDateTime,SubListID,ECategory,OrigName,Address,DOB,Nationality,Designation,Organization,Title,DO_Inclusion,DO_Exclusion,Aliases,Matching,ListType,Remarks,Status,Gender,Last_Occupation,Documents,Name FROM tbl_reviewentities WHERE SubListID = {} and SubListID is not null; """.format(sublistid)) 
                cursor_hbldelta.execute("""DELETE FROM tbl_reviewentities WHERE SubListID = {}""".format(sublistid)) 
                data = 'data updated successfully'
            if status == 'reject':
                Subwatchlist.objects.filter(sublistid=sublistid).update(status='declined',lastmodified=datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
                subwatchlist = Subwatchlist.objects.get(sublistid=sublistid)
                file_to_send = subwatchlist.filename
                file_to_send = 'http://142.93.92.13:8030' + file_to_send
                ############email sending/////////////////////////
                msg_body = """\
                <html>
                  <body>
                    <p>Hi Team,</p>
                    <p>The draft of {} has been <b>rejected</b> by the admin and is not updated.</p>
                    <p>Draft can be viewed on below mentioned link,</p>
                    <p>Link: <a href='{}'>{}</a></p>
                    <p></p>
                    <p>Regards,</p>
                    <p style="color:blue">Support Team</p>
                  </body>
                </html>
                """.format(subwatchlist.listid.program, file_to_send, subwatchlist.filename.split('/')[-1])
                message = MIMEMultipart()
                message['From'] = 'support team'
                message['To'] = 'lfdDeltagroup'
                message['Subject'] = 'Draft Rejected - DELTA'
                message.attach(MIMEText(msg_body,'html'))

                server = smtplib.SMTP('smtp.gmail.com:587')
                server.starttls()
                server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASS)
                server.sendmail(settings.EMAIL_HOST_USER,
                                    ['lfddelta@googlegroups.com'],
                                    message.as_string())
                server.quit()
                ############email sending/////////////////////////
                cursor_hbldelta.execute("""DELETE FROM tbl_reviewentities WHERE SubListID = {}""".format(sublistid))  
                data = 'data removed successfully'
            error = None
            data = data
            error_code = None
            return Response({'error': error, 'error_code': error_code, 'data': data},status=200)
        else:  
            error = serializer.errors
            data = None
            error_code = {'code':'400'}
            return Response({'error': error, 'error_code': error_code, 'data': data} , status=400)   




