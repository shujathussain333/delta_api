from django.views.generic.base import TemplateView
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view(['POST'])
def endpoint_disabled(request):
    error_message = 'Not Found'
    return Response({'detail': error_message}, status=status.HTTP_404_NOT_FOUND)


class ResetForgottenPassword(TemplateView):
    template_name = "auth/reset_forgotten_password.html"