from api.requestloger import db_logger
import time 
class RequestMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request, *kwargs):
        start_time = time.time()
        response = self.get_response(request)
        status = response.status_code
        duration = time.time() - start_time
        duration = round(duration,2)
        # if request.user.is_authenticated:
        db_logger(request,duration,status)
        return response
