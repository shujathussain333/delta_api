import re
from django.core.exceptions import ValidationError

def validate_hostname(hostname):
    if len(hostname) > 256:
        raise ValidationError('Invalid hostname', params={'hostname': hostname})
    if hostname[-1] == ".":
        hostname = hostname[:-1] # strip exactly one dot from the right, if present
    allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
    if not all(allowed.match(x) for x in hostname.split(".")):
        raise ValidationError('Invalid hostname', params={'hostname': hostname})