from django.db import models
from api.validators import validate_hostname
from datetime import datetime
from django.utils import timezone

class Role(models.Model):
    
    name = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name
    
class File(models.Model):
    file = models.FileField(upload_to='uploaded_files',blank=False, null=False)

class Watchlist(models.Model):
    listid = models.AutoField(db_column='ListID', primary_key=True)  # Field name made lowercase.
    program = models.CharField(db_column='Program', max_length=255, blank=True, null=True)  # Field name made lowercase.
    publishername = models.CharField(db_column='PublisherName', max_length=255, blank=True, null=True)  # Field name made lowercase.
    createddate = models.DateTimeField(db_column='CreatedDate', default=timezone.now)  # Field name made lowercase.
    
    class Meta:
        managed = True
        db_table = 'tbl_watchlist'


class Subwatchlist(models.Model):
    sublistid = models.AutoField(db_column='SubListID', primary_key=True)  # Field name made lowercase.
    version = models.IntegerField(db_column='Version')  # Field name made lowercase.
    lastmodified = models.DateTimeField(db_column='LastModified', default=timezone.now)  # Field name made lowercase.
    createddate = models.DateTimeField(db_column='CreatedDate', default=timezone.now)  # Field name made lowercase.
    isenabled = models.BooleanField(db_column='IsEnabled', default=True)  # Field name made lowercase.
    listid = models.ForeignKey('Watchlist', models.DO_NOTHING, db_column='ListID', blank=True, null=True) # Field name made lowercase.
    filename = models.CharField(db_column='Filename', max_length=255, blank=True, null=True)
    status = models.CharField(db_column='Status', max_length=255, blank=True, null=True)
    
    class Meta:
        managed = True
        db_table = 'tbl_subwatchlist'

    #def save(self, *args, **kwargs):
    #    self.lastmodified= datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    #    super(Subwatchlist, self).save(*args, **kwargs)


class ReviewEntities(models.Model):
    eid = models.AutoField(db_column='EID', primary_key=True)  # Field name made lowercase.
    edatetime = models.DateTimeField(db_column='EDateTime', default=timezone.now, null=True)  # Field name made lowercase.
    sublistid = models.ForeignKey('Subwatchlist', models.DO_NOTHING, db_column='SubListID', blank=True, null=True)  # Field name made lowercase.
    ecategory = models.CharField(db_column='ECategory', max_length=255, blank=True, null=True)  # Field name made lowercase.
    origname = models.TextField(db_column='OrigName', blank=True, null=True)  # Field name made lowercase.
    address = models.TextField(db_column='Address', blank=True, null=True)  # Field name made lowercase.
    dob = models.TextField(db_column='DOB', blank=True, null=True)  # Field name made lowercase.
    nationality = models.CharField(db_column='Nationality', max_length=255, blank=True, null=True)  # Field name made lowercase.
    designation = models.TextField(db_column='Designation', blank=True, null=True)  # Field name made lowercase.
    organization = models.CharField(db_column='Organization', max_length=255, blank=True, null=True)  # Field name made lowercase.
    title = models.TextField(db_column='Title', blank=True, null=True)  # Field name made lowercase.
    do_inclusion = models.DateField(db_column='DO_Inclusion', blank=True, null=True)  # Field name made lowercase.
    do_exclusion = models.DateField(db_column='DO_Exclusion', blank=True, null=True)  # Field name made lowercase.
    aliases = models.TextField(db_column='Aliases', blank=True, null=True)  # Field name made lowercase.
    matching = models.TextField(db_column='Matching', blank=True, null=True)  # Field name made lowercase.
    listtype = models.CharField(db_column='ListType', max_length=255, blank=True, null=True)  # Field name made lowercase.
    remarks = models.TextField(db_column='Remarks', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=50, blank=True, null=True)  # Field name made lowercase.
    gender = models.CharField(db_column='Gender', max_length=50, blank=True, null=True)  # Field name made lowercase.
    last_occupation = models.CharField(db_column='Last_Occupation', max_length=50, blank=True, null=True)  # Field name made lowercase.
    documents = models.TextField(db_column='Documents', blank=True, null=True)  # Field name made lowercase.
    name = models.TextField(db_column='Name', blank=True, null=True)  # Field name made lowercase.
    program = models.TextField(db_column='Program', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = True
        db_table = 'tbl_reviewentities'



class ApprovedEntities(models.Model):
    eid = models.AutoField(db_column='EID', primary_key=True)  # Field name made lowercase.
    edatetime = models.DateTimeField(db_column='EDateTime', default=timezone.now, blank=True, null=True)  # Field name made lowercase.
    sublistid = models.ForeignKey('Subwatchlist', models.DO_NOTHING, db_column='SubListID', blank=True, null=True)  # Field name made lowercase.
    ecategory = models.CharField(db_column='ECategory', max_length=255, blank=True, null=True)  # Field name made lowercase.
    origname = models.TextField(db_column='OrigName', blank=True, null=True)  # Field name made lowercase.
    address = models.TextField(db_column='Address', blank=True, null=True)  # Field name made lowercase.
    dob = models.TextField(db_column='DOB', blank=True, null=True)  # Field name made lowercase.
    nationality = models.CharField(db_column='Nationality', max_length=255, blank=True, null=True)  # Field name made lowercase.
    designation = models.TextField(db_column='Designation', blank=True, null=True)  # Field name made lowercase.
    organization = models.CharField(db_column='Organization', max_length=255, blank=True, null=True)  # Field name made lowercase.
    title = models.TextField(db_column='Title', blank=True, null=True)  # Field name made lowercase.
    do_inclusion = models.DateField(db_column='DO_Inclusion', blank=True, null=True)  # Field name made lowercase.
    do_exclusion = models.DateField(db_column='DO_Exclusion', blank=True, null=True)  # Field name made lowercase.
    aliases = models.TextField(db_column='Aliases', blank=True, null=True)  # Field name made lowercase.
    matching = models.TextField(db_column='Matching', blank=True, null=True)  # Field name made lowercase.
    listtype = models.CharField(db_column='ListType', max_length=255, blank=True, null=True)  # Field name made lowercase.
    remarks = models.TextField(db_column='Remarks', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=50, blank=True, null=True)  # Field name made lowercase.
    gender = models.CharField(db_column='Gender', max_length=50, blank=True, null=True)  # Field name made lowercase.
    last_occupation = models.CharField(db_column='Last_Occupation', max_length=50, blank=True, null=True)  # Field name made lowercase.
    documents = models.TextField(db_column='Documents', blank=True, null=True)  # Field name made lowercase.
    name = models.TextField(db_column='Name', blank=True, null=True)  # Field name made lowercase.
    program = models.TextField(db_column='Program', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = True
        db_table = 'tbl_approvedentities'




class HistoricalEntities(models.Model):
    eid = models.AutoField(db_column='EID', primary_key=True)  # Field name made lowercase.
    edatetime = models.DateTimeField(db_column='EDateTime', default=timezone.now, blank=True, null=True)  # Field name made lowercase.
    sublistid = models.ForeignKey('Subwatchlist', models.DO_NOTHING, db_column='SubListID', blank=True, null=True)  # Field name made lowercase.
    ecategory = models.CharField(db_column='ECategory', max_length=255, blank=True, null=True)  # Field name made lowercase.
    origname = models.TextField(db_column='OrigName', blank=True, null=True)  # Field name made lowercase.
    address = models.TextField(db_column='Address', blank=True, null=True)  # Field name made lowercase.
    dob = models.TextField(db_column='DOB', blank=True, null=True)  # Field name made lowercase.
    nationality = models.CharField(db_column='Nationality', max_length=255, blank=True, null=True)  # Field name made lowercase.
    designation = models.TextField(db_column='Designation', blank=True, null=True)  # Field name made lowercase.
    organization = models.CharField(db_column='Organization', max_length=255, blank=True, null=True)  # Field name made lowercase.
    title = models.TextField(db_column='Title', blank=True, null=True)  # Field name made lowercase.
    do_inclusion = models.DateField(db_column='DO_Inclusion', blank=True, null=True)  # Field name made lowercase.
    do_exclusion = models.DateField(db_column='DO_Exclusion', blank=True, null=True)  # Field name made lowercase.
    aliases = models.TextField(db_column='Aliases', blank=True, null=True)  # Field name made lowercase.
    matching = models.TextField(db_column='Matching', blank=True, null=True)  # Field name made lowercase.
    listtype = models.CharField(db_column='ListType', max_length=255, blank=True, null=True)  # Field name made lowercase.
    remarks = models.TextField(db_column='Remarks', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='Status', max_length=50, blank=True, null=True)  # Field name made lowercase.
    gender = models.CharField(db_column='Gender', max_length=50, blank=True, null=True)  # Field name made lowercase.
    last_occupation = models.CharField(db_column='Last_Occupation', max_length=50, blank=True, null=True)  # Field name made lowercase.
    documents = models.TextField(db_column='Documents', blank=True, null=True)  # Field name made lowercase.
    name = models.TextField(db_column='Name', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = True
        db_table = 'tbl_historicalentities'

class RequestLogs(models.Model):
    activity_name = models.CharField(max_length=500)
    url = models.CharField(max_length=500)
    view_name = models.CharField(max_length=50, null=True,blank=True)
    method = models.CharField(max_length=6)
    query_params = models.CharField(null=True, blank=True,max_length=500)
    form_data = models.CharField(null=True, blank=True,max_length=500)
    created_by = models.CharField(max_length=100, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    response_time = models.CharField(null=True, blank=True,max_length=500)
    class Meta:
        managed = True
        db_table = 'tbl_requestlogs'    


class UploadedFiles(models.Model):
    fileid = models.AutoField(db_column='FileID', primary_key=True)  # Field name made lowercase.
    filename = models.CharField(db_column='Filename', max_length=255, blank=True, null=True)            


