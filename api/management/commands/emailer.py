from django.core.management.base import BaseCommand
from django.conf import settings
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
class Command(BaseCommand):
    def handle(self, *args, **options):
        ############email sending/////////////////////////
        msg_body = """\
        <html>
          <body>
            <p>Hi Team,</p>
            <p>The draft is available to be pushed for client, kindly login and confirm so that data can pushed.</p>  
            <p></p>
            <p>Regards,</p>
            <p style="color:blue">Support Team</p>                  
          </body>
        </html>
        """
        message = MIMEMultipart()
        message['From'] = 'support team'
        message['To'] = 'lfdDeltagroup'
        message['Subject'] = 'Awaiting Response - DELTA'
        message.attach(MIMEText(msg_body,'html'))

        server = smtplib.SMTP('smtp.gmail.com:587')
        server.starttls()
        server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASS)
        server.sendmail(settings.EMAIL_HOST_USER,
                            ['lfddelta@googlegroups.com'],
                            message.as_string())
        server.quit()
        ############email sending/////////////////////////