from django.core.management.base import BaseCommand
import pandas as  pd
from sqlalchemy import create_engine
from decouple import config
from django.db import connections, connection
from decouple import config
from api.tagger.delta import Tagger
import time
from api.models import *
from decouple import config
import os 
import re
from datetime import datetime


class Command(BaseCommand):
    def handle(self, *args, **options):
        cleaned_cnic = []
        file_name = config('MEDIA_CSVS_PATH') + '/uploaded_files/inhouse_maintained_lists.xlsx'
        hbl_df1 = pd.read_excel(file_name,'ATA Letter Details',usecols=['CNIC Number'])
        hbl_df1.rename(columns={"CNIC Number":"CNIC"},inplace=True)        
        hbl_df2 = pd.read_excel(file_name,'CTD',usecols=['CNIC '])
        hbl_df2.rename(columns={"CNIC ":"CNIC"},inplace=True)
        hbl_df3 = pd.read_excel(file_name,'FIA RED BOOK - Terrorist',usecols=['CNIC '])
        hbl_df3.rename(columns={"CNIC ":"CNIC"},inplace=True)
        hbl_df4 = pd.read_excel(file_name,'FIA RED BOOK - HT',usecols=['CNIC '])
        hbl_df4.rename(columns={"CNIC ":"CNIC"},inplace=True)
        hbl_df5 = pd.read_excel(file_name,'Court Order',usecols=['CNIC'])
        hbl_df5.rename(columns={"CNIC":"CNIC"},inplace=True)
        hbl_df6 = pd.read_excel(file_name,'Associates',usecols=['CNIC Number/ Passport No.'])
        hbl_df6.rename(columns={"CNIC Number/ Passport No.":"CNIC"},inplace=True)
        hbl_df7 = pd.read_excel(file_name,'Plea Bargin',usecols=['CNIC'])
        hbl_df7.rename(columns={"CNIC":"CNIC"},inplace=True)

        comp_hbl_df = pd.concat([hbl_df1,hbl_df2,hbl_df3,hbl_df4,hbl_df5,hbl_df6,hbl_df7], ignore_index=True)
        comp_hbl_df.drop_duplicates(subset=None, keep='first', inplace=True)
        for rcn in comp_hbl_df['CNIC']:
          rcnic = str(rcn)
          rcnic = rcnic.replace('.0','')
          if '/' in rcnic:
            mrnc = rcnic.split('/') 
            for ernc in mrnc:
              cnic_detail = [c for c in ernc if c.isdigit()]
              cnic_detail = ''.join(cnic_detail).strip() 
              if len(cnic_detail)==13 and cnic_detail[0] in ['1','2','3','4','5','6','7']:
                cleaned_cnic.append(cnic_detail)
          elif ';' in rcnic:
            mrnc = rcnic.split(';') 
            for ernc in mrnc:
              cnic_detail = [c for c in ernc if c.isdigit()]
              cnic_detail = ''.join(cnic_detail).strip() 
              if len(cnic_detail)==13 and cnic_detail[0] in ['1','2','3','4','5','6','7']:
                cleaned_cnic.append(cnic_detail)
          else:
            cnic_detail = [c for c in rcnic if c.isdigit()]
            cnic_detail = ''.join(cnic_detail).strip() 
            if len(cnic_detail)==13 and cnic_detail[0] in ['1','2','3','4','5','6','7']:
              cleaned_cnic.append(cnic_detail)                      

        timestr = time.strftime("%d-%m-%Y")



        file_name_delta_sanction = config('MEDIA_CSVS_PATH') + '/uploaded_files/Delta_sanctions_{}.xlsx'.format(timestr)       
        today_df = pd.read_excel(file_name_delta_sanction)

        if not os.path.exists(config('MEDIA_CSVS_PATH') + '/delta_dump'):
            os.makedirs(config('MEDIA_CSVS_PATH') + '/delta_dump')                        
        file_name_dump = config('MEDIA_CSVS_PATH') + '/delta_dump/delta_dump.xlsx'
        try:
          comp_df = pd.read_excel(file_name_dump)
          comp_df.drop_duplicates(subset=None, keep='first', inplace=True)
          comp_df = pd.concat([today_df, comp_df], ignore_index=True)
          comp_df.to_excel(file_name_dump,encoding='utf-8', index=False)
        except:
          comp_df = today_df
          comp_df.drop_duplicates(subset=None, keep='first', inplace=True)
          comp_df.to_excel(file_name_dump,encoding='utf-8', index=False)
        comp_df.dropna(subset=['Uid'],inplace=True)
        print(len(comp_df),'befor///////////////')
        comp_df = comp_df.loc[~comp_df['CNIC'].isin(cleaned_cnic)]
        comp_df = comp_df.loc[~comp_df['Doc No'].isin(cleaned_cnic)] 
        print(len(comp_df),'after====================')
        comp_df.drop_duplicates(subset=None, keep='first', inplace=True) 
        comp_df.drop_duplicates(subset='Uid', keep='first', inplace=True)
        
        if not os.path.exists(config('MEDIA_CSVS_PATH') + '/delta'):
            os.makedirs(config('MEDIA_CSVS_PATH') + '/delta')                        
        file_name_dsanction = config('MEDIA_CSVS_PATH') + '/delta/Delta_sanctions_{}.xlsx'.format(timestr)
        comp_df.to_excel(file_name_dsanction, encoding='utf-8', index=False,columns=['Category','Entered Date','Deleted Date','Sub-Category','Uid','Doc Type','Doc No','Father Name','Address','Updated','E-i','Title','Position','Company Name','First Name','Last Name','CNIC','Alias','Alias2','Age','As_of_Date','DOB','Deceased Date','Further Information','Country','Place of Birth','City','Citizenship','Keyword','Companies','Linked to','External_Sources'])
        

        file_name_delta_criminals = config('MEDIA_CSVS_PATH') + '/uploaded_files/Delta_criminals_{}.xlsx'.format(timestr)      
        today_criminal_df = pd.read_excel(file_name_delta_criminals)

        if not os.path.exists(config('MEDIA_CSVS_PATH') + '/delta_criminal_dump'):
            os.makedirs(config('MEDIA_CSVS_PATH') + '/delta_criminal_dump')                        
        file_name_criminal_dump = config('MEDIA_CSVS_PATH') + '/delta_criminal_dump/delta_criminal_dump.xlsx'
        try:
          comp_df_criminals = pd.read_excel(file_name_criminal_dump)
          comp_df_criminals = pd.concat([today_criminal_df, comp_df_criminals], ignore_index=True)
          comp_df_criminals.to_excel(file_name_criminal_dump,encoding='utf-8', index=False)
        except:
          comp_df_criminals = today_criminal_df
          comp_df_criminals.to_excel(file_name_criminal_dump,encoding='utf-8', index=False)

        comp_df_criminals.dropna(subset=['Uid'],inplace=True)          
        comp_df_criminals = comp_df_criminals.loc[~comp_df_criminals['CNIC'].isin(cleaned_cnic)]
        comp_df_criminals = comp_df_criminals.loc[~comp_df_criminals['Doc No'].isin(cleaned_cnic)]
        comp_df_criminals.drop_duplicates(subset=None, keep='first', inplace=True)
        comp_df_criminals = comp_df_criminals.loc[(comp_df_criminals['Keyword'] =='Sindh Police')]
        comp_df_criminals.drop_duplicates(subset=None, keep='first', inplace=True) 
        comp_df_criminals.drop_duplicates(subset='Uid', keep='first', inplace=True)        

        file_name2 = config('MEDIA_CSVS_PATH') + '/delta/Delta_criminals_{}.xlsx'.format(timestr)
        comp_df_criminals.to_excel(file_name2, encoding='utf-8', index=False,columns=['Category','Entered Date','Deleted Date','Sub-Category','Uid','Doc Type','Doc No','Father Name','Address','Updated','E-i','Title','Position','Company Name','First Name','Last Name','CNIC','Alias','Alias2','Age','As_of_Date','DOB','Deceased Date','Further Information','Country','Place of Birth','City','Citizenship','Keyword','Companies','Linked to','External_Sources'])
        
        if not os.path.exists(config('MEDIA_CSVS_PATH') + '/denotified'):
            os.makedirs(config('MEDIA_CSVS_PATH') + '/denotified')              
        file_name3 = file_name = config('MEDIA_CSVS_PATH') + '/denotified/delta_denotified_{}.xlsx'.format(timestr)
        comp_df['Deleted Date'] = pd.to_datetime(comp_df['Deleted Date'])
        denotified_comp_df = comp_df.loc[comp_df['Deleted Date'] <= datetime.date(datetime.today())]
        denotified_comp_df.to_excel(file_name3, encoding='utf-8', index=False,columns=['Category','Entered Date','Deleted Date','Sub-Category','Uid','Doc Type','Doc No','Father Name','Address','Updated','E-i','Title','Position','Company Name','First Name','Last Name','CNIC','Alias','Alias2','Age','As_of_Date','DOB','Deceased Date','Further Information','Country','Place of Birth','City','Citizenship','Keyword','Companies','Linked to','External_Sources'])
        