from django.core.management.base import BaseCommand, CommandError
from api.models import Client
from api.helpers import relations


class Command(BaseCommand):
    help = 'Assigns watchlists to clients who have no prior assignments'

    def handle(self, *args, **options):
        empty_clients = Client.objects.filter(lists__isnull=True)
        
        for client in empty_clients:
            self.stdout.write(self.style.SUCCESS('Processing {}'.format(client.name)))
            relations.assign_watchlists(client)

        self.stdout.write(self.style.SUCCESS('Successfully assigned lists to clients'))