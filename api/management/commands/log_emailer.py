from django.core.management.base import BaseCommand
from django.conf import settings
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from decouple import config
import time
import smtplib
class Command(BaseCommand):
    def handle(self, *args, **options):
        timestr = time.strftime("%d-%m-%Y")
        loger_file = 'media/logs/logs_{}.xlsx'.format(timestr)
        file_to_send = 'http://142.93.92.13:8030/'+ loger_file
        ############email sending/////////////////////////
        msg_body = """\
        <html>
          <body>
            <p>Hi Team,</p>
            <p>The week log is available to view</p>
            <p>Log can be viewed on below mentioned link,</p>
            <p>Link: <a href='{}'>File</a></p>
            <p></p>
            <p>Regards,</p>
            <p style="color:blue">Support Team</p>                  
          </body>
        </html>
        """.format(file_to_send)
        message = MIMEMultipart()
        message['From'] = 'support team'
        message['To'] = 'lfdDeltagroup'
        message['Subject'] = 'Week Logs - DELTA'
        message.attach(MIMEText(msg_body,'html'))

        server = smtplib.SMTP('smtp.gmail.com:587')
        server.starttls()
        server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASS)
        server.sendmail(settings.EMAIL_HOST_USER,
                            ['lfddelta@googlegroups.com'],
                            message.as_string())
        server.quit()
        ############email sending/////////////////////////