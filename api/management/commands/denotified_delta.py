from django.core.management.base import BaseCommand
import pandas as  pd
from sqlalchemy import create_engine
from decouple import config
from django.db import connections, connection
from decouple import config
from api.tagger.delta import Tagger
import time
from api.models import *
from decouple import config
import os 
import re
from datetime import datetime


class Command(BaseCommand):
    def handle(self, *args, **options):                      

        timestr = time.strftime("%d-%m-%Y")
        cursor_hbluqaab = connections['hblUqaab_db'].cursor()
        sub_list_id5 = Subwatchlist.objects.filter(filename__contains='delta_denotified').order_by("-version")[0]

        sql_query_delta_denotified = pd.read_sql_query("""select distinct ECategory,tbl_subwatchlist.Program,OrigName,Address,DOB,Nationality,Designation,Organization ,Title
           ,DO_Inclusion
           ,DO_Exclusion
           ,Aliases
           ,Matching
           ,tbl_entities.ListType
           ,Remarks
           ,Status
           ,Gender
           ,Last_Occupation
           ,Documents
           ,Name
           ,tbl_entities.EID
           from tbl_entities 
       inner join tbl_subwatchlist on tbl_subwatchlist.sublistid = tbl_entities.sublistid 
           where program = 'OFAC-SDN Denotified'""", connections['hblUqaab_db'])
        df_delta_denotified = pd.DataFrame(sql_query_delta_denotified, columns=['ECategory','Program','OrigName','Address','DOB','Nationality','Designation','Organization','Title'
           ,'DO_Inclusion'
           ,'DO_Exclusion'
           ,'Aliases'
           ,'Matching'
           ,'ListType'
           ,'Remarks'
           ,'Status'
           ,'Gender'
           ,'Last_Occupation'
           ,'Documents'
           ,'Name'
           ,'EID'])

        
        df_delta_denotified.insert(0, 'SubListID', str(sub_list_id5.sublistid))
         
        query=''' INSERT INTO tbl_reviewentities
            (SubListID,ECategory,Program,OrigName,Address,DOB,Nationality,Designation,Organization,Title,DO_Inclusion,DO_Exclusion,Aliases,Matching,ListType,Remarks,Status,Gender,Last_Occupation,Documents,Name,U_id) 
        VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) '''
        cursor = connections['default'].cursor()
        cursor.executemany(query, df_delta_denotified.to_records(index=False).tolist())        
        comp_df = df_delta_denotified  

        comp_df.insert(0, 'Doc Type', str(' '))
        comp_df.insert(0, 'Doc No', str(' ')) 
        comp_df.insert(0, 'Father Name', str(' '))
        comp_df.insert(0, 'Company Name', str(' '))        
        comp_df.insert(0,'City',str(' '))
        comp_df.insert(0, 'updated', str(' '))
        comp_df.insert(0,'first_name',str(' '))
        comp_df.insert(0,'last_name',str(' '))
        comp_df.insert(0,'alias2',str(' '))
        comp_df.insert(0,'age',str(' '))
        comp_df.insert(0,'as_of_date',str(' '))
        comp_df.insert(0,'deceased',str(' '))
        comp_df.insert(0,'place_of_birth',str(' '))
        comp_df.insert(0,'citizenship',str(' '))
        comp_df.insert(0,'linked_to',str(' '))
        comp_df.insert(0,'external_sources',str(' '))
        comp_df.insert(0,'CNIC',str(' '))
        comp_df.insert(0,'Extra CNIC',str(' '))
        comp_df['EID'] = comp_df['EID'].apply(lambda x: 'D'+str(x))

        a_alias_info = []
        a_address_info = []
        title_list = []
        pob_list = []
        alias2_list = []
        dob_info = []
        first_name_list = []
        last_name_list = []
        cnic_info = []
        comp_name = []
        doc_type_list = []
        doc_number_list = []
        address_info = []
        father_name_list = []
        extra_cnic_list = []

        for alias in comp_df['Aliases']:
          if '{"info": null}' in alias:
            alias = alias.replace('null',"'Null'")
            alias = eval(alias)
            a_alias_info.append(' ')
            alias2_list.append(' ')
          else:
            all_akas = ''
            aka = None
            alias = eval(alias)
            aka_detail = alias['info']
            for aka_ifo in aka_detail:
              aka = aka_ifo['aka'].strip()
              all_akas +=aka + '  ,' 
            try:  
              all_akas = all_akas.split(',')
              a_alias_info.append(','.join(all_akas[0:-3]))
              alias2_list.append(all_akas[-2])
            except:
              all_akas = all_akas.split(',')
              a_alias_info.append(','.join(all_akas[0:-3]))
              alias2_list.append(all_akas[-2])                
                 
        comp_df['Aliases'] = a_alias_info
        comp_df['alias2'] = alias2_list

        for alias in comp_df['Title']:
          if '{"info": null}' in alias:
            alias = alias.replace('null',"'Null'")
            alias = eval(alias)
            title_list.append(' ')
          else:
            all_akas = []
            aka = None
            alias = eval(alias)
            aka_detail = alias['info']
            for aka_ifo in aka_detail:
              aka = aka_ifo['title'].strip()
              all_akas.append(aka)
            all_akas = ','.join(all_akas)
            title_list.append(all_akas)
        
        comp_df['Title'] = title_list

        for alias in comp_df['Address']:
          if '{"address info": null' in alias:
            alias = alias.replace('null',"'Null'")
            alias = eval(alias)
            a_address_info.append(' ')
            address_info.append(' ')
          else:
            alias = alias.replace('null',"'Null'")
            city = None
            alias = eval(alias)
            address_detail = alias['address info']
            city = address_detail[0]['city'].strip()
            address = address_detail[0]['address'].strip()
            if city == 'Null':
              a_address_info.append(' ')
            else:
              try:
                city = city.replace('City','')
              except:
                city = city  
              a_address_info.append(city)
            if address == 'Null':
              address_info.append(' ')
            else:
              address_info.append(address)     
        comp_df['Address'] = address_info  
        comp_df['City'] = a_address_info

        for alias in comp_df['DOB']:
          if '{"info": null}' in alias:
            alias = alias.replace('null',"'Null'")
            alias = eval(alias)
            dob_info.append(' ')
            pob_list.append(' ')
          else:
              alias = alias.replace('null',"'Null'")
              alias = eval(alias)
              dob_details = alias['info']
              dob = dob_details[0]['DOB'].strip()
              pob = dob_details[0]['POB'].strip()
              
              if pob != 'Null':
                pob_list.append(pob)
              else:
                pob_list.append(' ')
              if dob != 'Null':
                if '00:00:00' in dob:
                  dob = dob.replace('00:00:00','')
                  dob = dob.replace('-','/')
                elif '-' in dob:
                  dob = dob.replace('-','/')
                elif ',' in dob :
                  dob = ' '.join(dob.split(',',2)[:2])
                  dob = dob.replace(',','')
                  dob = datetime.strptime(dob, '%B %d %Y')
                  dob = dob.strftime('%d/%m/%Y')
                elif '00/00/' in dob:
                  dob = dob.replace('00/00/','')
                else:
                  dob = dob
                try:
                  dob = dob.strip()
                  dob = datetime.strptime(dob, "%Y/%m/%d")
                  dob = dob.strftime('%d/%m/%Y')                    
                  dob_info.append(dob)
                except:
                  dob = dob.strip()
                  dob_info.append(dob)                  
                     
              else:
                dob_info.append(' ')
                   
        comp_df['DOB'] = dob_info 
        comp_df['place_of_birth'] = pob_list                

        for index,row in comp_df.iterrows():
          if row['ECategory'] == 'SIP':
            temp = row['ListType']
            temp1 = row['ECategory']
            row['ECategory'] = temp
            row['ListType'] = temp1
            if row['ECategory'] == 'Group':
              row['ECategory'] = 'Entity'
          elif row['ECategory'] == 'Group':
            row['ECategory'] = 'Entity'

          if row['ECategory']=='Individual':
            name = str(row['Name'])
            first_last_name = name.split()
            first = first_last_name[0]
            last = ' '.join(first_last_name[1:])
            try:
              first = re.sub('\d','',first) 
              first_name_list.append(first)
            except:
              first_name_list.append(first) 
            try: 
              last = re.sub('\d','',last) 
              last_name_list.append(last)
            except:
              last_name_list.append(last)
            comp_name.append(' ')
          else:
            name = str(row['Name'])
            comp_name.append(name)
            first_name_list.append(' ')
            last_name_list.append(' ')

        comp_df['first_name'] = first_name_list
        comp_df['last_name'] = last_name_list    
        comp_df['Company Name'] = comp_name

        for alias in comp_df['Documents']:
          if '{"document_info": null}' in alias:
            alias = alias.replace('null',"'Null'")
            alias = eval(alias)
            cnic_info.append(' ')
            doc_type_list.append(' ')
            doc_number_list.append(' ')
            extra_cnic_list.append(' ')
          else:
              alias = alias.replace('null',"'Null'")
              cnic_detail = None
              psport = None
              alias = eval(alias)
              doc_details = alias['document_info']
              for doc in doc_details:
                if doc['type'] in ['National Identification Number' , 'national identification no','NI Number','identification_Number','CNIC']:
                  cnic_detail = doc['id']
                  cnic_detail = [c for c in cnic_detail if c.isspace() or c.isdigit()]
                  cnic_detail = ''.join(cnic_detail).strip()
                  try:
                    cnic_detail = max(re.findall(r'\d+', cnic_detail), key = len) 
                  except:
                    cnic_detail = None
                elif doc['type'] in ['Passport Number' , 'passport']:
                  if isinstance(doc['id'], list):
                    psport = doc['id'][0]
                    psport = [c for c in psport if c.isspace() or c.isdigit() or c.isupper()]
                    psport = ''.join(psport).strip()
                    try:
                      psport = max(re.findall(r'[A-Z]+\d+', psport), key = len)
                    except:
                      try:
                        psport = max(re.findall(r'\d+', psport), key = len) 
                      except:
                        psport = None 
                  elif isinstance(doc['id'], str):
                    psport = doc['id']
                    psport = [c for c in psport if c.isspace() or c.isdigit() or c.isupper()]
                    psport = ''.join(psport).strip()
                    try:
                      psport = max(re.findall(r'[A-Z]+\d+', psport), key = len)
                    except:
                      try:
                        psport = max(re.findall(r'\d+', psport), key = len) 
                      except:
                        psport = None   

              if cnic_detail is not None and psport is None:
                if len(cnic_detail)==13 and cnic_detail[0] in ['1','2','3','4','5','6','7']:
                  cnic_info.append(' ')
                  doc_number_list.append(cnic_detail)
                  doc_type_list.append('CNIC')
                  extra_cnic_list.append(cnic_detail)
                else:
                  cnic_info.append(str(' '))
                  doc_number_list.append(str(' ')) 
                  doc_type_list.append(str(' '))
                  extra_cnic_list.append(str(' '))                  
              elif psport is not None and cnic_detail is None:
                  cnic_info.append(str(' '))
                  doc_number_list.append(psport) 
                  doc_type_list.append('Passport')
                  extra_cnic_list.append(str(' '))
              elif cnic_detail and psport:
                  if len(cnic_detail)==13 and cnic_detail[0] in ['1','2','3','4','5','6','7']:
                    cnic_info.append(cnic_detail)
                    doc_number_list.append(psport) 
                    doc_type_list.append('Passport')
                    extra_cnic_list.append(cnic_detail)
                  else:
                    cnic_info.append(str(' '))
                    doc_number_list.append(psport) 
                    doc_type_list.append('Passport')                    
                    extra_cnic_list.append(str(' '))                        
              elif cnic_detail is None and psport is None:
                    cnic_info.append(str(' '))
                    doc_number_list.append(str(' ')) 
                    doc_type_list.append(str(' '))
                    extra_cnic_list.append(str(' '))                                    
                                                         
        comp_df['CNIC'] = cnic_info 
        comp_df['Doc Type'] = doc_type_list
        comp_df['Doc No'] = doc_number_list
        comp_df['Extra CNIC'] = extra_cnic_list
        comp_df['DO_Inclusion'] = comp_df['DO_Inclusion'].apply(lambda x:x if str(x)[0]!='0' else ' ')
        comp_df['ECategory'] = comp_df['ECategory'].apply(lambda x:x if str(x)!='Group' else 'Entity')

        # print(len(comp_df),'befor///////////////')
        # comp_df = comp_df.loc[~comp_df['Extra CNIC'].isin(cleaned_cnic)] 
        # comp_df = comp_df.loc[(comp_df['first_name']!='(NOT')&(comp_df['last_name']!='CLEAR)')]        
        # print(len(comp_df),'after====================')
        comp_df.rename(columns={"Name":"Full Name","Aliases": "Alias","ECategory":"Category","DO_Inclusion":"entered","DO_Exclusion":"deleted","Gender":"e-i","Last_Occupation":"Position","Remarks":"further_information","Nationality":"country","Program":"keyword","Organization":"companies","ListType":"sub-category","EID":"uid"},inplace=True)
        comp_df.drop_duplicates(subset=None, keep='first', inplace=True)                   
        if not os.path.exists(config('MEDIA_CSVS_PATH') + '/denotified'):
            os.makedirs(config('MEDIA_CSVS_PATH') + '/denotified')              
        file_name = file_name = config('MEDIA_CSVS_PATH') + '/denotified/delta_denotified_{}.xlsx'.format(timestr)
        comp_df.to_excel(file_name, encoding='utf-8', index=False,columns=['Category','entered','deleted','sub-category','uid','Doc Type','Doc No','Father Name','Address','updated','e-i','Title','Position','Company Name','first_name','last_name','CNIC','Alias','alias2','age','as_of_date','DOB','deceased','further_information','country','place_of_birth','City','citizenship','keyword','companies','linked_to','external_sources'])




