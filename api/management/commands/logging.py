from django.core.management.base import BaseCommand
import pandas as  pd
from sqlalchemy import create_engine
from decouple import config
from django.db import connections, connection
from decouple import config
from api.tagger.delta import Tagger
import time
from api.models import *
from django.db.models import Max
from decouple import config
import json 
import os

class Command(BaseCommand):
    def handle(self, *args, **options):
        timestr = time.strftime("%d-%m-%Y")
        cursor_hbldelta = connections['default'].cursor()
        sql_query_logs = pd.read_sql_query("""SELECT * FROM tbl_requestlogs""", connections['default'])
        df_logs = pd.DataFrame(sql_query_logs)
        if not os.path.exists(config('MEDIA_CSVS_PATH') + '/logs'):
            os.makedirs(config('MEDIA_CSVS_PATH') + '/logs')          
        file_name = config('MEDIA_CSVS_PATH') + '/logs/logs_{}.xlsx'.format(timestr)

        df_logs = df_logs[df_logs.activity_name != 'File download/']
        df_logs = df_logs[df_logs.activity_name != '']
        df_logs.to_excel(file_name, encoding='utf-8', index=False,columns=['activity_name','created_by','created_at','response_time'])