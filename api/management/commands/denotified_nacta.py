from django.core.management.base import BaseCommand
import pandas as  pd
from sqlalchemy import create_engine
from decouple import config
from django.db import connections, connection
from decouple import config
from api.tagger.delta import Tagger
import time
from api.models import *
from decouple import config
import os
import numpy as np
import csv
import re
import json

pd.set_option('display.float_format', lambda x: '%.2f' % x)
class Command(BaseCommand):
    def handle(self, *args, **options):
        timestr = time.strftime("%d-%m-%Y")
        cursor_hbluqaab = connections['hblUqaab_db'].cursor()
        sub_list_id4= Subwatchlist.objects.filter(filename__contains='nacta_denotified').order_by("-version")[0]
        sql_query_nacta = pd.read_sql_query("""select distinct entity_type,date_of_birth,nationality
           ,date_of_inclusion
           ,date_of_exclusion
           ,aliases
           ,info
           ,gender
           ,Documents
           ,cleaned_name
           ,prefix_name
           ,postfix_name
           ,id
            from entities where scrapper_tag = 'UN1XTK1594207302'""", connections['hblUqaab_db'])
        df_denotified_nacta = pd.DataFrame(sql_query_nacta,columns=['entity_type','date_of_birth','nationality'
           ,'date_of_inclusion'
           ,'date_of_exclusion'
           ,'aliases'
           ,'info'
           ,'gender'
           ,'documents'
           ,'cleaned_name'
           ,'prefix_name'
           ,'postfix_name'     
           ,'id'])

        df_denotified_nacta = df_denotified_nacta.replace(np.nan, '', regex=True)
        df_denotified_nacta['cleaned_name'] = df_denotified_nacta['prefix_name'].astype(str)+' '+df_denotified_nacta['cleaned_name']+' '+df_denotified_nacta['postfix_name']        
        df_denotified_nacta.rename(columns={"cleaned_name": "Name","aliases": "Aliases","entity_type":"ECategory","id":"EID","date_of_inclusion":"DO_Inclusion","date_of_exclusion":"DO_Exclusion","gender":"Gender","info":"Remarks","date_of_birth":"DOB","documents":"Documents"},inplace=True)


        df_denotified_nacta.insert(0, 'SubListID', str(sub_list_id4.sublistid))
        #query=''' INSERT INTO tbl_reviewentities
            #(SubListID,ECategory,OrigName,Address,DOB,Nationality,Designation,Organization,Title,DO_Inclusion,DO_Exclusion,Aliases,Matching,ListType,Remarks,Status,Gender,Last_Occupation,Documents,Name) 
        #VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) '''
        #cursor = connections['default'].cursor()
        #cursor.executemany(query, df_denotified_nacta.to_records(index=False).tolist())
        df_denotified_nacta.insert(0, 'Programs', str('Nacta'))
        df_denotified_nacta.insert(0,'Father Name',str(' '))
        df_denotified_nacta.insert(0,'Province',str(' '))
        df_denotified_nacta.insert(0,'Sub-Category',str('DEN'))
        df_denotified_nacta.insert(0, 'Doc Type', str('CNIC'))
        df_denotified_nacta.insert(0, 'Doc No', str(' ')) 
        df_denotified_nacta.insert(0,'CNIC',str(' '))         
        df_denotified_nacta.insert(0, 'Updated', str(' '))  
        df_denotified_nacta.insert(0, 'Company Name', str(' '))
        df_denotified_nacta.insert(0,'First Name',str(' '))
        df_denotified_nacta.insert(0,'Last Name',str(' ')) 
        df_denotified_nacta.insert(0,'Alias2',str(' ')) 
        df_denotified_nacta.insert(0,'Age',str(' '))
        df_denotified_nacta.insert(0,'As_of_Date',str(' '))
        df_denotified_nacta.insert(0,'Deceased Date',str(' '))
        df_denotified_nacta.insert(0,'Place of Birth',str(' '))
        df_denotified_nacta.insert(0,'City',str(' ')) 
        df_denotified_nacta.insert(0,'Citizenship',str(' '))
        df_denotified_nacta.insert(0,'Linked to',str(' '))
        df_denotified_nacta.insert(0,'External_Sources',str(' ')) 



        df_denotified_nacta['EID'] = df_denotified_nacta['EID'].apply(lambda x: 'D'+str(x))

        #df_denotified_nacta['Name'] = df_denotified_nacta['Name'].apply(lambda x:" ".join(x.split()))
        a_alias_info = []
        a_address_info = []
        dob_info = []
        father_name =[]
        cnic_info = []
        country_info = []
        province_info = []
        first_name_list = []
        last_name_list = []
        comp_name = []  
        alias2_list = []
        pob_list = []
        city_info = []
        title_list = []                              

        for alias in df_denotified_nacta['Aliases']:
          if alias:
            try:
              alises = alias.split(',')
              a_alias_info.append(aliases[0])
              alias2_list.append(','.join(aliases[1:]))
            except:
              a_alias_info.append(alias)
              alias2_list.append(' ')   
      
          else:
            a_alias_info.append(' ')
            alias2_list.append(' ')                
                 
        df_denotified_nacta['Aliases'] = a_alias_info
        df_denotified_nacta['Alias2'] = alias2_list
        df_denotified_nacta['Aliases'] = df_denotified_nacta['Aliases'].str.replace(r"[()]","").str.strip()

        for alias in df_denotified_nacta['Remarks']:
          if alias:
            provience = alias['personal_details']['province']
            province_info.append(provience)
            city = alias['personal_details']['city']
            city_info.append(city)
            father_nam = alias['personal_details']['father_name']
            father_name.append(father_nam)
            a_address_info.append(' ')
            country_info.append(' ')
            pob_list.append(' ')
            title_list.append(' ')      
          else:  
            provience = ' '
            province_info.append(provience)
            city = ' '
            city_info.append(city)
            father_nam = ' '
            father_name.append(father_nam)      
            a_address_info.append(' ')
            country_info.append(' ')
            pob_list.append(' ')
            title_list.append(' ')        
        df_denotified_nacta['Address'] = a_address_info  
        df_denotified_nacta['Country'] = country_info
        df_denotified_nacta['Province'] = province_info
        df_denotified_nacta['City'] = city_info
        df_denotified_nacta['Father Name'] = father_name    
        df_denotified_nacta['Father Name'] = df_denotified_nacta['Father Name'].str.replace(r"[()]","").str.strip()
        df_denotified_nacta['Father Name'] = df_denotified_nacta['Father Name'].str.upper()
        df_denotified_nacta['Place of Birth'] = pob_list 
        df_denotified_nacta['Title'] = title_list
        
        for alias in df_denotified_nacta['DOB']:
          if alias:
            dob_info.append(alias)
          else:
              dob_info.append(' ')
        df_denotified_nacta['DOB'] = dob_info

        for alias in df_denotified_nacta['Documents']:
          if alias:
            try:
              cnic = alias['national_identification_number']
              cnic_info.append(cnic)
            except:
              cnic_info.append(' ')
          else:
              cnic_info.append(' ')
        df_nacta['Doc No'] = cnic_info

        for index,row in df_denotified_nacta.iterrows():
          if row['ECategory']=='IND':
            name = str(row['Name'])     
            try:
              name_aliases = re.match(r'\((.*)\)', name).group(1)
              row['Aliases'] = name_aliases
              row['Name'] = row['Name'].replace('({})'.format(name_aliases),'').strip()
              name = str(row['Name'])              
              first_last_name = name.split()
              first = first_last_name[0]
              last = ' '.join(first_last_name[1:]) 
              first_name_list.append(first)
              last_name_list.append(last)             
            except:
              row['Name'] = row['Name']
              name_check = str(row['Name']) 
              if 'ALIAS' in name_check:         
                row['Name'] = name_check.split('ALIAS')[0]
                row['Aliases'] = name_check.split('ALIAS')[1]
              name = str(row['Name'])       
              first_last_name = name.split()
              first = first_last_name[0]
              last = ' '.join(first_last_name[1:]) 
              first_name_list.append(first)
              last_name_list.append(last)               
              row['Aliases'] = row['Aliases']  
            comp_name.append(' ')
          else:
            name = str(row['Name'])
            comp_name.append(name)
            first_name_list.append(' ')
            last_name_list.append(' ')

        df_denotified_nacta['First Name'] = first_name_list
        df_denotified_nacta['First Name'] = df_denotified_nacta['First Name'].str.upper()
        df_denotified_nacta['Last Name'] = last_name_list 
        df_denotified_nacta['Last Name'] = df_denotified_nacta['Last Name'].str.upper() 
        df_denotified_nacta['Last Name'] = df_denotified_nacta['Last Name'].str.replace("ALIAS"," ").str.strip()  
        df_denotified_nacta['Company Name'] = comp_name      
        df_denotified_nacta['Name'] = df_denotified_nacta['Name'].str.upper()
        df_denotified_nacta['Aliases'] = df_denotified_nacta['Aliases'].str.upper()
        df_denotified_nacta['Name'] = df_denotified_nacta['Name'].str.replace("ALIAS"," ").str.strip()
        df_denotified_nacta['Alias2'] = df_denotified_nacta['Alias2'].str.upper()   
        df_denotified_nacta['City'] = df_denotified_nacta['City'].str.title()


        for index,row in df_denotified_nacta.iterrows():

          province_name = str(row['Province'])
          if province_name == 'KP':
            row['Province'] = 'Khyber Pakhtunkhwa'
          elif province_name == 'AJ&K': 
            row['Province'] ='Azad Jammu and Kashmir'
          elif province_name == 'ICT': 
            row['Province'] ='Islamabad Capital Territory' 
          elif province_name == 'Gilgit-baltistan': 
            row['Province'] ='Gilgit Baltistan'                        
          else:
            row['Province'] = str(row['Province']).title() 



        df_denotified_nacta.rename(columns={"Name": "Full Name","Aliases": "Alias","ECategory":"Category","EID":"Uid","DO_Inclusion":"Entered Date","DO_Exclusion":"Deleted Date","Gender":"E-i","Last_Occupation":"Position","Programs":"Keyword","Remarks":"Further Information","Organization":"Companies"},inplace=True)
        df_denotified_nacta['Further Information'] = df_denotified_nacta['Further Information'].apply(lambda x: json.dumps(x))
        df_denotified_nacta['Documents'] = df_denotified_nacta['Documents'].apply(lambda x: json.dumps(x))        
        df_denotified_nacta.drop_duplicates(subset=None, keep='first', inplace=True)
        df_denotified_nacta['Further Information'] = df_denotified_nacta['Further Information'].apply(lambda x: ' ')
        if not os.path.exists(config('MEDIA_CSVS_PATH') + '/denotified'):
            os.makedirs(config('MEDIA_CSVS_PATH') + '/denotified')              
        file_name = file_name = config('MEDIA_CSVS_PATH') + '/denotified/nacta_denotified_{}.xlsx'.format(timestr)
        df_denotified_nacta.to_excel(file_name, encoding='utf-8', index=False, columns=['Category','Entered Date','Deleted Date','Sub-Category','Uid','Doc Type','Doc No','Father Name','Address','Updated','E-i','Title','Position','Company Name','First Name','Last Name','Full Name','CNIC','Alias','Alias2','Age','As_of_Date','DOB','Deceased Date','Further Information','Country','Place of Birth','City','Citizenship','Keyword','Companies','Linked to','External_Sources','Province']) 
