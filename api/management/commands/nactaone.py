from django.core.management.base import BaseCommand
import pandas as  pd
from sqlalchemy import create_engine
from decouple import config
from django.db import connections, connection
from decouple import config
from api.tagger.delta import Tagger
import time
from api.models import *
from decouple import config
import os
import numpy as np
import csv
import re

class Command(BaseCommand):
    def handle(self, *args, **options):
        timestr = time.strftime("%d-%m-%Y")
        cursor_hbluqaab = connections['hblUqaab_db'].cursor()
        sub_list_id1= Subwatchlist.objects.filter(filename__contains='Schedule1').order_by("-version")[0]
        sql_query_nacta_org = pd.read_sql_query("""select distinct entity_type,date_of_birth,nationality
           ,date_of_inclusion
           ,date_of_exclusion
           ,aliases
           ,info
           ,gender
           ,Documents
           ,scrapped_name
           ,id
            from entities where scrapper_tag = '132UN71583388557'""", connections['hblUqaab_db'])
        df_nacta_org = pd.DataFrame(sql_query_nacta_org,columns=['entity_type','date_of_birth','nationality'
           ,'date_of_inclusion'
           ,'date_of_exclusion'
           ,'aliases'
           ,'info'
           ,'gender'
           ,'documents'
           ,'scrapped_name'
           ,'id'])

        df_nacta_org.rename(columns={"scrapped_name": "Name","aliases": "Aliases","entity_type":"ECategory","id":"EID","date_of_inclusion":"DO_Inclusion","date_of_exclusion":"DO_Exclusion","gender":"Gender","info":"Remarks","date_of_birth":"DOB","documents":"Documents"},inplace=True)              
        df_nacta_org.insert(0, 'SubListID', str(sub_list_id1.sublistid))
        #query=''' INSERT INTO tbl_reviewentities
            #(SubListID,ECategory,OrigName,Address,DOB,Nationality,Designation,Organization,Title,DO_Inclusion,DO_Exclusion,Aliases,Matching,ListType,Remarks,Status,Gender,Last_Occupation,Documents,Name,U_id) 
        #VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) '''
        #cursor = connections['default'].cursor()
        #cursor.executemany(query, df_nacta_org.to_records(index=False).tolist())
        df_nacta_org.insert(0, 'Programs', str('Nacta Organization'))
        df_nacta_org.insert(0,'Father Name',str(' '))
        df_nacta_org.insert(0,'Province',str(' '))
        df_nacta_org.insert(0,'Sub-Category',str('SIP'))
        df_nacta_org.insert(0, 'Doc Type', str(' '))
        df_nacta_org.insert(0, 'Doc No', str(' ')) 
        df_nacta_org.insert(0,'CNIC',str(' '))         
        df_nacta_org.insert(0, 'Updated', str(' '))  
        df_nacta_org.insert(0, 'Company Name', str(' '))
        df_nacta_org.insert(0,'First Name',str(' '))
        df_nacta_org.insert(0,'Last Name',str(' ')) 
        df_nacta_org.insert(0,'Alias2',str(' ')) 
        df_nacta_org.insert(0,'Age',str(' '))
        df_nacta_org.insert(0,'As_of_Date',str(' '))
        df_nacta_org.insert(0,'Deceased Date',str(' '))
        df_nacta_org.insert(0,'Place of Birth',str(' '))
        df_nacta_org.insert(0,'City',str(' ')) 
        df_nacta_org.insert(0,'Citizenship',str(' '))
        df_nacta_org.insert(0,'Linked to',str(' '))
        df_nacta_org.insert(0,'External_Sources',str(' ')) 
        df_nacta_org['EID'] = df_nacta_org['EID'].apply(lambda x: 'D'+str(x))
        df_nacta_org['ECategory'] = df_nacta_org['ECategory'].apply(lambda x: 'Entity' if x=='Group' else x)
        df_nacta_org['Name'] = df_nacta_org['Name'].apply(lambda x:" ".join(x.split()))
        df_nacta_org['Name'] = df_nacta_org['Name'].str.upper()
        df_nacta_org['Name'] = df_nacta_org['Name'].str.replace(r",","").str.strip()        
        df_nacta_org['Name'] = df_nacta_org['Name'].str.replace(r"AFFILIATED ORGANIZATIONS:","").str.strip()
        df_nacta_org['Name'] = df_nacta_org['Name'].str.replace(r"I+[.]|II+[.]|III+[.]|IV+[.]|V+[.]|VI+[.]|VII+[.]",",").str.strip()		
        a_alias_info = []
        a_address_info = []
        dob_info = []
        father_name =[]
        cnic_info = []
        country_info = []
        province_info = []
        first_name_list = []
        last_name_list = []
        comp_name = []  
        alias2_list = []
        pob_list = []
        city_info = []
        title_list = [] 
        temp_alias_list = []                             

        for alias in df_nacta_org['Aliases']:
          if alias:
            a_alias_info.append(' ')
            alias2_list.append(' ')
          else:
            a_alias_info.append(' ')
            alias2_list.append(' ')     
                 
        df_nacta_org['Aliases'] = a_alias_info
        df_nacta_org['Alias2'] = alias2_list
        df_nacta_org['Aliases'] = df_nacta_org['Aliases'].str.replace(r"[()]","").str.strip()

        for alias in df_nacta_org['Remarks']:
          a_address_info.append(' ')
          country_info.append(' ')
          province_info.append(' ')
          city_info.append(' ')
          pob_list.append(' ')
          father_name.append(' ')
          title_list.append(' ')      
        df_nacta_org['Address'] = a_address_info  
        df_nacta_org['Country'] = country_info
        df_nacta_org['Province'] = province_info
        df_nacta_org['City'] = city_info
        df_nacta_org['Place of Birth'] = pob_list
        df_nacta_org['Father Name'] = father_name
        df_nacta_org['Title'] = title_list    
        
        for alias in df_nacta_org['DOB']:
          dob_info.append(' ')
        df_nacta_org['DOB'] = dob_info 

        
        for alias in df_nacta_org['Documents']:
          cnic_info.append(' ')
        df_nacta_org['Doc No'] = cnic_info

        for index,row in df_nacta_org.iterrows():
          if row['ECategory']=='Individual':
            name = str(row['Name'])
            try:
              name_aliases = re.match(r'\((.*)\)', name).group(1)
              row['Aliases'] = name_aliases
              row['Name'] = row['Name'].replace('({})'.format(name_aliases),'').strip()
              name = str(row['Name'])              
              first_last_name = name.split()
              first = first_last_name[0]
              last = ' '.join(first_last_name[1:]) 
              first_name_list.append(first)
              last_name_list.append(last)             
            except:
              row['Name'] = row['Name']
              name = str(row['Name'])              
              first_last_name = name.split()
              first = first_last_name[0]
              last = ' '.join(first_last_name[1:]) 
              first_name_list.append(first)
              last_name_list.append(last)               
              row['Aliases'] = row['Aliases']  
            comp_name.append(' ')
          else:
            name = str(row['Name'])
            try:
              name_aliases = name[name.find("(")+1:name.find(")")]
              temp_alias_list.append(name_aliases)
              row['Name'] = row['Name'].replace('({})'.format(name_aliases),'').strip()
              name = str(row['Name'])
              comp_name.append(name)
            except:
              comp_name.append(name)
              temp_alias_list.append(' ')  			
            first_name_list.append(' ')
            last_name_list.append(' ')
        df_nacta_org['First Name'] = first_name_list
        df_nacta_org['First Name'] = df_nacta_org['First Name'].str.upper()
        df_nacta_org['Last Name'] = last_name_list 
        df_nacta_org['Last Name'] = df_nacta_org['Last Name'].str.upper()   
        df_nacta_org['Company Name'] = comp_name 
        df_nacta_org['Company Name'] = df_nacta_org['Company Name'].str.replace("/"," ").str.strip()    
        df_nacta_org['Name'] = df_nacta_org['Company Name']
        df_nacta_org['Aliases'] = temp_alias_list
        df_nacta_org['Aliases'] = df_nacta_org['Aliases'].str.upper()


        for index,row in df_nacta_org.iterrows():

          province_name = str(row['Province'])
          if province_name == 'KP':
            row['Province'] = 'Khyber Pakhtunkhwa'
          elif province_name == 'AJ&K': 
            row['Province'] ='Azad Jammu and Kashmir'
          elif province_name == 'ICT': 
            row['Province'] ='Islamabad Capital Territory' 
          elif province_name == 'Gilgit-baltistan': 
            row['Province'] ='Gilgit Baltistan'                        
          else:
            row['Province'] = str(row['Province']).title() 

        df_nacta_org['Company Name'] = df_nacta_org['Company Name'].str.split(',')
        # convert list of pd.Series then stack it
        df_nacta_org = (df_nacta_org.set_index(['External_Sources', 'Linked to', 'Citizenship', 'City',
       'Place of Birth', 'Deceased Date', 'As_of_Date', 'Age', 'Alias2',
       'Last Name', 'First Name', 'Updated', 'CNIC', 'Doc No',
       'Doc Type', 'Sub-Category', 'Province', 'Father Name', 'Programs',
       'SubListID', 'ECategory', 'DOB', 'nationality', 'DO_Inclusion',
       'DO_Exclusion', 'Aliases', 'Remarks', 'Gender', 'Documents', 'Name',
       'EID', 'Address', 'Country', 'Title'])['Company Name'].apply(pd.Series).stack().drop(columns=['level_34']).reset_index().rename(columns={0:'Company Name'}))		
        #df_nacta_org['Name'] = df_nacta_org['Company Name']
        df_nacta_org.rename(columns={"Name": "Full Name","Aliases": "Alias","ECategory":"Category","EID":"Uid","DO_Inclusion":"Entered Date","DO_Exclusion":"Deleted Date","Gender":"E-i","Last_Occupation":"Position","Programs":"Keyword","Remarks":"Further Information","Organization":"Companies"},inplace=True)
        df_nacta_org.drop_duplicates(subset=None, keep='first', inplace=True)      
        if not os.path.exists(config('MEDIA_CSVS_PATH') + '/nacta_one'):
            os.makedirs(config('MEDIA_CSVS_PATH') + '/nacta_one')          
        file_name = config('MEDIA_CSVS_PATH') + '/nacta_one/Schedule1_{}.xlsx'.format(timestr)
        df_nacta_org.to_excel(file_name, encoding='utf-8', index=False, columns=['Category','Entered Date','Deleted Date','Sub-Category','Uid','Doc Type','Doc No','Father Name','Address','Updated','E-i','Title','Position','Company Name','First Name','Last Name','Full Name','CNIC','Alias','Alias2','Age','As_of_Date','DOB','Deceased Date','Further Information','Country','Place of Birth','City','Citizenship','Keyword','Companies','Linked to','External_Sources','Province'])
       