from django.core.management.base import BaseCommand
import pandas as  pd
from sqlalchemy import create_engine
from decouple import config
from django.db import connections, connection
from decouple import config
from api.tagger.delta import Tagger
import time
import os
from api.models import *

class Command(BaseCommand):

    def handle(self, *args, **options):
        timestr = time.strftime("%d-%m-%Y")
        with open("varstore.dat", "a+") as f:
            f.seek(0)
            val = int(f.read() or 0) + 1
            f.seek(0)
            f.truncate()
            f.write(str(val)) 
            version_mange = val

        cursor_hbldelta = connections['default'].cursor()

        listid1 = Watchlist.objects.get(program='Delta Sanctions List')
        listid2 = Watchlist.objects.get(program='NACTA Schedule I')
        listid3 = Watchlist.objects.get(program='NACTA Schedule IV')
        listid4 = Watchlist.objects.get(program='NACTA Denotified List')
        listid5 = Watchlist.objects.get(program='Delta Denotified List')
        listid6 = Watchlist.objects.get(program='Delta Criminals List')

        b = Subwatchlist(version= version_mange,isenabled='1',filename ='/media/delta/Delta_sanctions_{}.xlsx'.format(timestr),listid = listid1, status = 'draft')
        b.save() 

        b1 = Subwatchlist(version= version_mange,isenabled='1',filename ='/media/nacta_one/Schedule1_{}.xlsx'.format(timestr),listid = listid2, status = 'draft')
        b1.save()

        b2 = Subwatchlist(version= version_mange,isenabled='1',filename ='/media/nacta_four/Schedule4_{}.xlsx'.format(timestr),listid = listid3, status = 'draft')
        b2.save() 

        b3 = Subwatchlist(version= version_mange,isenabled='1',filename ='/media/denotified/nacta_denotified_{}.xlsx'.format(timestr),listid = listid4, status = 'draft')
        b3.save() 

        b4 = Subwatchlist(version= version_mange,isenabled='1',filename ='/media/denotified/delta_denotified_{}.xlsx'.format(timestr),listid = listid5, status = 'draft')
        b4.save()              

        b5 = Subwatchlist(version= version_mange,isenabled='1',filename ='/media/delta/Delta_criminals_{}.xlsx'.format(timestr),listid = listid6, status = 'draft')
        b5.save()








        





        
        



               