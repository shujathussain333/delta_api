from django.core.management.base import BaseCommand
import pandas as  pd
from sqlalchemy import create_engine
from decouple import config
from django.db import connections, connection
from decouple import config
from api.tagger.delta import Tagger
import time
from api.models import *
from decouple import config
import os
import numpy as np
import csv
from django.conf import settings
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from decouple import config
import time
import smtplib

pd.set_option('display.float_format', lambda x: '%.2f' % x)
class Command(BaseCommand):
    def handle(self, *args, **options):
        timestr = time.strftime("%d-%m-%Y")      
        file_name = config('MEDIA_CSVS_PATH') + '/nacta_four/Schedule4_{}.xlsx'.format(timestr)   
        nacta_df = pd.read_excel(file_name)
        nacta_df = nacta_df.replace(np.nan, '', regex=True)
        
        file_name_denotified = config('MEDIA_CSVS_PATH') + '/denotified/nacta_denotified_{}.xlsx'.format(timestr)
        denotified_nacta_df = pd.read_excel(file_name_denotified)
        # denotified_nacta_df = denotified_nacta_df.replace(np.nan, '', regex=True)
        # df_merge = pd.DataFrame(columns=nacta_df.columns)
        # comp_df = pd.concat([nacta_df, denotified_nacta_df])
        # duplicates_df = comp_df[comp_df.duplicated(keep=False)]
        # match_rows = duplicates_df.groupby(duplicates_df.columns.tolist()).apply(lambda x: tuple(x.index)).tolist()
        # match_rows = [match_rows[i][0] for i in range(len(match_rows))]
        # for index in match_rows:
        #   df_merge = df_merge.append(nacta_df.iloc[index])
        # df_merge.reset_index(drop=True, inplace=True)

        if not os.path.exists(config('MEDIA_CSVS_PATH') + '/dail_denotified'):
            os.makedirs(config('MEDIA_CSVS_PATH') + '/dail_denotified')              
        file_name_match = config('MEDIA_CSVS_PATH') + '/dail_denotified/nacta_denotified_{}.xlsx'.format(timestr)
        denotified_nacta_df.to_excel(file_name_match, encoding='utf-8', index=False, columns=['Full Name','Father Name','AKA Full Name','CNIC','Address','Province','Country','Programs','DOB','Nationality'])        
        
        denotified_file = 'media/dail_denotified/nacta_denotified_{}.xlsx'.format(timestr)
        file_to_send = 'http://142.93.92.13:8030/'+ denotified_file
        ############email sending/////////////////////////
        msg_body = """\
        <html>
          <body>
            <p>Hi Team,</p>
            <p>Nacta denotified list is available</p>
            <p>file can be viewed on below mentioned link,</p>
            <p>Link: <a href='{}'>File</a></p>
            <p></p>
            <p>Regards,</p>
            <p style="color:blue">Support Team</p>                  
          </body>
        </html>
        """.format(file_to_send)
        message = MIMEMultipart()
        message['From'] = 'support team'
        message['To'] = 'lfdDeltagroup'
        message['Subject'] = 'Denotified - NACTA'
        message.attach(MIMEText(msg_body,'html'))

        server = smtplib.SMTP('smtp.gmail.com:587')
        server.starttls()
        server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASS)
        server.sendmail(settings.EMAIL_HOST_USER,
                            ['lfddelta@googlegroups.com'],
                            message.as_string())
        server.quit()
        ############email sending/////////////////////////        







