from api.models import RequestLogs
from datetime import datetime
import json
def db_logger(request,duration,status):

    try:
        file_name = request.POST['file_name']
    except:
        file_name = ''       
    activity = ''
    if request.path == '/auth/jwt/create/' and status==200:
    	activity = 'Login'

    else:
        if request.path == '/logout':
            activity = 'Logout'
        else:	
            activity = 'File download/{}'.format(file_name) 

    if request.path == '/auth/jwt/create/' and status!=200:
        activity = 'Invalid User'		

    try:
        user = 	request.POST['email']
    except:
        try:
        	user = request.user
        except:
            user = 'Anonymous'		
    RequestLogs.objects.create(
        url=request.path,
        # view_name=request.resolver_match.func.__name__,
        method=request.method,
        query_params=request.GET,
        created_by=user,
        response_time = duration,
        activity_name = activity
    )
