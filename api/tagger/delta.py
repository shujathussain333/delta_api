import pandas as pd
import sqlalchemy as sa
import json
import time
import re 

class Tagger():
    def result(self, df):
        cities_re = """baluchistan|\\fata\\|\\bkhyber pakhtunkhwa\\b|pakistan|\\bpk\\b|\\btando-jan-muhammadb|\\btando jan\\b|\\babbotabad\\b|\\babbottabad\\b|\\baj&k\\b|\\baliabad\\b|\\balpurai\\b|\\balpuri\\b|\\bathmuqam\\b|\\battock\\b|\\bawaran\\b|\\bbadin\\b|\\bbahawalnagar\\b|\\bbahawalpur\\b|\\bbalochistan\\b|\\bbaltistan\\b|\\bbannu\\b|\\bbardar\\b|\\bbarian pachiot rawalakot\\b|\\bbarkhan\\b|\\bbatagram\\b|\\bbatgram\\b|\\bbattagram\\b|\\bbattgram\\b|\\bbhakkar\\b|\\bbhathal gunjari balooch\\b|\\bbhettik baloch\\b|\\bbolan\\b|\\bbuner\\b|\\bchaghi\\b|\\bchak dhamni\\b|\\bchakwal\\b|\\bcharsada\\b|\\bcharsadda\\b|\\bchilas\\b|\\bchinatt\\b|\\bchiniot\\b|\\bchitral\\b|\\bd.g.khan\\b|\\bd.i.khan\\b|\\bd.m.jamali\\b|\\bdaadu\\b|\\bdadu\\b|\\bdaggar\\b|\\bdalbandin\\b|\\bdanna papa nar\\b|\\bdasu\\b|\\bdatoot rawalakot\\b|\\bdera allahyar\\b|\\bdera bugti\\b|\\bdera ghazi khan\\b|\\bdera ismail khan\\b|\\bdera murad jamali\\b|\\bdgkhan\\b|\\bdhadar\\b|\\bdhirkot bagh\\b|\\bdiamir\\b|\\bdikhan\\b|\\bdir lower\\b|\\bdistrict bagh\\b|\\bdistrict poonch\\b|\\bdral naka gojra\\b|\\beidgah\\b|\\bfaisalabad\\b|\\bgakuch\\b|\\bgandava\\b|\\bgandawah\\b|\\bgawadar\\b|\\bghanche\\b|\\bghizer\\b|\\bghotki\\b|\\bgilgit\\b|\\bgilgit-baltistan\\b|\\bgujranwala\\b|\\bgujrat\\b|\\bgwadar\\b|\\bhafizabad\\b|\\bhangu\\b|\\bhari gehl\\b|\\bhari gehl bagh\\b|\\bharipur\\b|\\bharnai\\b|\\bharyala danna kacheli\\b|\\bhernai\\b|\\bhunza\\b|\\bhyderabad\\b|\\bislamabad\\b|\\bjacobabad\\b|\\bjaffarabad\\b|\\bjamshoro\\b|\\bjandali\\b|\\bjhal magsi\\b|\\bjhal magsi gandawah\\b|\\bjhandala\\b|\\bjhang\\b|\\bjhang sadr\\b|\\bjhelum\\b|\\bkachhi\\b|\\bkafl garh bagh\\b|\\bkalat\\b|\\bkalaya\\b|\\bkambar\\b|\\bkandhkot\\b|\\bkarachi\\b|\\bkarak\\b|\\bkarim abad\\b|\\bkasur\\b|\\bkhaal\\b|\\bkhairpur\\b|\\bkhanewal\\b|\\bkhaplu\\b|\\bkharan\\b|\\bkharipur\\b|\\bkharmang\\b|\\bkhirk\\b|\\bkhushab\\b|\\bkhuzdar\\b|\\bkhyber\\b|\\bkilla saifullah\\b|\\bkohat\\b|\\bkohlu\\b|\\bkotli\\b|\\bkundian\\b|\\blahore\\b|\\blakki\\b|\\blakki marwat\\b|\\blarkana\\b|\\blasbela\\b|\\blayyah\\b|\\bleiah\\b|\\blodhran\\b|\\bloralai\\b|\\blower dir\\b|\\bm.b.din\\b|\\bmakri\\b|\\bmalakand\\b|\\bmandi bahauddin\\b|\\bmansehra\\b|\\bmardan\\b|\\bmasiwala\\b|\\bmastung\\b|\\bmatiari\\b|\\bmehra\\b|\\bmian channu\\b|\\bmianwali\\b|\\bmirpur\\b|\\bmirpur khas\\b|\\bmirpurkhas\\b|\\bmuhmand agency\\b|\\bmultan\\b|\\bmusa khel bazar\\b|\\bmuzaffarabad\\b|\\bmuzaffargarh\\b|\\bn-feroze\\b|\\bnakar dana sudhnoti\\b|\\bnankana sahib\\b|\\bnarowal\\b|\\bnasirabad\\b|\\bnaushahro firoz\\b|\\bnawabshah\\b|\\bnorth waziristan agency\\b|\\bnoshki\\b|\\bnowshera\\b|\\bnushki\\b|\\bokara\\b|\\borakzai\\b|\\bpakpattan\\b|\\bpanjgur\\b|\\bparachinar\\b|\\bpeshawar\\b|\\bpidhar mato khan\\b|\\bpishin\\b|\\bpoonch\\b|\\bpunjab\\b|\\bqila abdullah\\b|\\bqila saifullah\\b|\\bqilla saifullah\\b|\\bquetta\\b|\\brahim yaar khan\\b|\\brahim yar khan\\b|\\brahimyar khan\\b|\\brajanpur\\b|\\brawala kot\\b|\\brawalakot\\b|\\brawalpindi\\b|\\brawli dhokan\\b|\\bsadiqabad\\b|\\bsahiwal\\b|\\bsaidu sharif\\b|\\b\x08sakhar\x08\\b|\\bsangahr\\b|\\bsanghar\\b|\\bsargodha\\b|\\bsawat\\b|\\bserai\\b|\\bseryian\\b|\\bshaeed benizrabad\\b|\\bshahdad kot\\b|\\bshahdadkot\\b|\\bshaheed benazirabad\\b|\\bshangla\\b|\\bsheikhupura\\b|\\bshigar\\b|\\bshikarpur\\b|\\bsialkot\\b|\\b\x08sibbi\x08\\b|\\b\x08sibi\x08\\b|\\bsindh\\b|\\bskardu\\b|\\bsohbatpur\\b|\\bsudhnoti\\b|\\bsukkur\\b|\\bswabi\\b|\\bswat\\b|\\bt.t.singh\\b|\\btando allah yar\\b|\\btando allahyar\\b|\\btando muhammad khan\\b|\\btemergara\\b|\\bthatta\\b|\\btimargara\\b|\\btoba tek singh\\b|\\btoorghar\\b|\\btorghar\\b|\\bturbat\\b|\\bumarkot\\b|\\bumerkot\\b|\\bupper dir\\b|\\buthal\\b|\\bvehari\\b|\\bvihari\\b|\\bwashuk\\b|\\bwaziristan\\b|\\bzhob\\b|\\bziarat\\b"""    
                
        country_code = """\\bAF\\b|\\bAL\\b|\\bDZ\\b|\\bAS\\b|\\bAD\\b|\\bAO\\b|\\bAI\\b|\\bAQ\\b|\\bAG\\b|\\bAR\\b|\\bAM\\b|\\bAW\\b|\\bAU\\b|\\bAT\\b|\\bAZ\\b|\\bBS\\b|\\bBH\\b|\\bBD\\b|\\bBB\\b|\\bBY\\b|\\bBE\\b|\\bBZ\\b|\\bBJ\\b|\\bBM\\b|\\bBT\\b|\\bBO\\b|\\bBO\\b|\\bBA\\b|\\bBW\\b|\\bBV\\b|\\bBR\\b|\\bIO\\b|\\bBN\\b|\\bBN\\b|\\bBG\\b|\\bBF\\b|\\bBI\\b|\\bKH\\b|\\bCM\\b|\\bCA\\b|\\bCV\\b|\\bKY\\b|\\bCF\\b|\\bTD\\b|\\bCL\\b|\\bCN\\b|\\bCX\\b|\\bCC\\b|\\bCO\\b|\\bKM\\b|\\bCG\\b|\\bCD\\b|\\bCK\\b|\\bCR\\b|\\bCI\\b|\\bCI\\b|\\bHR\\b|\\bCU\\b|\\bCY\\b|\\bCZ\\b|\\bDK\\b|\\bDJ\\b|\\bDM\\b|\\bDO\\b|\\bEC\\b|\\bEG\\b|\\bSV\\b|\\bGQ\\b|\\bER\\b|\\bEE\\b|\\bET\\b|\\bFK\\b|\\bFO\\b|\\bFJ\\b|\\bFI\\b|\\bFR\\b|\\bGF\\b|\\bPF\\b|\\bTF\\b|\\bGA\\b|\\bGM\\b|\\bGE\\b|\\bDE\\b|\\bGH\\b|\\bGI\\b|\\bGR\\b|\\bGL\\b|\\bGD\\b|\\bGP\\b|\\bGU\\b|\\bGT\\b|\\bGG\\b|\\bGN\\b|\\bGW\\b|\\bGY\\b|\\bHT\\b|\\bHM\\b|\\bVA\\b|\\bHN\\b|\\bHK\\b|\\bHU\\b|\\bIS\\b|\\bIN\\b|\\bID\\b|\\bIR\\b|\\bIQ\\b|\\bIE\\b|\\bIM\\b|\\bIL\\b|\\bIT\\b|\\bJM\\b|\\bJP\\b|\\bJE\\b|\\bJO\\b|\\bKZ\\b|\\bKE\\b|\\bKI\\b|\\bKP\\b|\\bKR\\b|\\bKR\\b|\\bKW\\b|\\bKG\\b|\\bLA\\b|\\bLV\\b|\\bLB\\b|\\bLS\\b|\\bLR\\b|\\bLY\\b|\\bLY\\b|\\bLI\\b|\\bLT\\b|\\bLU\\b|\\bMO\\b|\\bMK\\b|\\bMG\\b|\\bMW\\b|\\bMY\\b|\\bMV\\b|\\bML\\b|\\bMT\\b|\\bMH\\b|\\bMQ\\b|\\bMR\\b|\\bMU\\b|\\bYT\\b|\\bMX\\b|\\bFM\\b|\\bMD\\b|\\bMC\\b|\\bMN\\b|\\bME\\b|\\bMS\\b|\\bMA\\b|\\bMZ\\b|\\bMM\\b|\\bMM\\b|\\bNA\\b|\\bNR\\b|\\bNP\\b|\\bNL\\b|\\bAN\\b|\\bNC\\b|\\bNZ\\b|\\bNI\\b|\\bNE\\b|\\bNG\\b|\\bNU\\b|\\bNF\\b|\\bMP\\b|\\bNO\\b|\\bOM\\b|\\bPW\\b|\\bPS\\b|\\bPA\\b|\\bPG\\b|\\bPY\\b|\\bPE\\b|\\bPH\\b|\\bPN\\b|\\bPL\\b|\\bPT\\b|\\bPR\\b|\\bQA\\b|\\bRE\\b|\\bRO\\b|\\bRU\\b|\\bRU\\b|\\bRW\\b|\\bSH\\b|\\bKN\\b|\\bLC\\b|\\bPM\\b|\\bVC\\b|\\bVC\\b|\\bVC\\b|\\bWS\\b|\\bSM\\b|\\bST\\b|\\bSA\\b|\\bSN\\b|\\bRS\\b|\\bSC\\b|\\bSL\\b|\\bSG\\b|\\bSK\\b|\\bSI\\b|\\bSB\\b|\\bSO\\b|\\bZA\\b|\\bGS\\b|\\bES\\b|\\bLK\\b|\\bSD\\b|\\bSR\\b|\\bSJ\\b|\\bSZ\\b|\\bSE\\b|\\bCH\\b|\\bSY\\b|\\bTW\\b|\\bTW\\b|\\bTJ\\b|\\bTZ\\b|\\bTH\\b|\\bTL\\b|\\bTG\\b|\\bTK\\b|\\bTO\\b|\\bTT\\b|\\bTT\\b|\\bTN\\b|\\bTR\\b|\\bTM\\b|\\bTC\\b|\\bTV\\b|\\bUG\\b|\\bUA\\b|\\bAE\\b|\\bGB\\b|\\bUS\\b|\\bUM\\b|\\bUY\\b|\\bUZ\\b|\\bVU\\b|\\bVE\\b|\\bVE\\b|\\bVN\\b|\\bVN\\b|\\bVG\\b|\\bVI\\b|\\bWF\\b|\\bEH\\b|\\bYE\\b|\\bZM\\b|\\bZW\\b|\\bAFG\\b|\\bALB\\b|\\bDZA\\b|\\bASM\\b|\\bAND\\b|\\bAGO\\b|\\bAIA\\b|\\bATA\\b|\\bATG\\b|\\bARG\\b|\\bARM\\b|\\bABW\\b|\\bAUS\\b|\\bAUT\\b|\\bAZE\\b|\\bBHS\\b|\\bBHR\\b|\\bBGD\\b|\\bBRB\\b|\\bBLR\\b|\\bBEL\\b|\\bBLZ\\b|\\bBEN\\b|\\bBMU\\b|\\bBTN\\b|\\bBOL\\b|\\bBOL\\b|\\bBIH\\b|\\bBWA\\b|\\bBVT\\b|\\bBRA\\b|\\bIOT\\b|\\bBRN\\b|\\bBRN\\b|\\bBGR\\b|\\bBFA\\b|\\bBDI\\b|\\bKHM\\b|\\bCMR\\b|\\bCAN\\b|\\bCPV\\b|\\bCYM\\b|\\bCAF\\b|\\bTCD\\b|\\bCHL\\b|\\bCHN\\b|\\bCXR\\b|\\bCCK\\b|\\bCOL\\b|\\bCOM\\b|\\bCOG\\b|\\bCOD\\b|\\bCOK\\b|\\bCRI\\b|\\bCIV\\b|\\bCIV\\b|\\bHRV\\b|\\bCUB\\b|\\bCYP\\b|\\bCZE\\b|\\bDNK\\b|\\bDJI\\b|\\bDMA\\b|\\bDOM\\b|\\bECU\\b|\\bEGY\\b|\\bSLV\\b|\\bGNQ\\b|\\bERI\\b|\\bEST\\b|\\bETH\\b|\\bFLK\\b|\\bFRO\\b|\\bFJI\\b|\\bFIN\\b|\\bFRA\\b|\\bGUF\\b|\\bPYF\\b|\\bATF\\b|\\bGAB\\b|\\bGMB\\b|\\bGEO\\b|\\bDEU\\b|\\bGHA\\b|\\bGIB\\b|\\bGRC\\b|\\bGRL\\b|\\bGRD\\b|\\bGLP\\b|\\bGUM\\b|\\bGTM\\b|\\bGGY\\b|\\bGIN\\b|\\bGNB\\b|\\bGUY\\b|\\bHTI\\b|\\bHMD\\b|\\bVAT\\b|\\bHND\\b|\\bHKG\\b|\\bHUN\\b|\\bISL\\b|\\bIND\\b|\\bIDN\\b|\\bIRN\\b|\\bIRQ\\b|\\bIRL\\b|\\bIMN\\b|\\bISR\\b|\\bITA\\b|\\bJAM\\b|\\bJPN\\b|\\bJEY\\b|\\bJOR\\b|\\bKAZ\\b|\\bKEN\\b|\\bKIR\\b|\\bPRK\\b|\\bKOR\\b|\\bKOR\\b|\\bKWT\\b|\\bKGZ\\b|\\bLAO\\b|\\bLVA\\b|\\bLBN\\b|\\bLSO\\b|\\bLBR\\b|\\bLBY\\b|\\bLBY\\b|\\bLIE\\b|\\bLTU\\b|\\bLUX\\b|\\bMAC\\b|\\bMKD\\b|\\bMDG\\b|\\bMWI\\b|\\bMYS\\b|\\bMDV\\b|\\bMLI\\b|\\bMLT\\b|\\bMHL\\b|\\bMTQ\\b|\\bMRT\\b|\\bMUS\\b|\\bMYT\\b|\\bMEX\\b|\\bFSM\\b|\\bMDA\\b|\\bMCO\\b|\\bMNG\\b|\\bMNE\\b|\\bMSR\\b|\\bMAR\\b|\\bMOZ\\b|\\bMMR\\b|\\bMMR\\b|\\bNAM\\b|\\bNRU\\b|\\bNPL\\b|\\bNLD\\b|\\bANT\\b|\\bNCL\\b|\\bNZL\\b|\\bNIC\\b|\\bNER\\b|\\bNGA\\b|\\bNIU\\b|\\bNFK\\b|\\bMNP\\b|\\bNOR\\b|\\bOMN\\b|\\bPLW\\b|\\bPSE\\b|\\bPAN\\b|\\bPNG\\b|\\bPRY\\b|\\bPER\\b|\\bPHL\\b|\\bPCN\\b|\\bPOL\\b|\\bPRT\\b|\\bPRI\\b|\\bQAT\\b|\\bREU\\b|\\bROU\\b|\\bRUS\\b|\\bRUS\\b|\\bRWA\\b|\\bSHN\\b|\\bKNA\\b|\\bLCA\\b|\\bSPM\\b|\\bVCT\\b|\\bVCT\\b|\\bVCT\\b|\\bWSM\\b|\\bSMR\\b|\\bSTP\\b|\\bSAU\\b|\\bSEN\\b|\\bSRB\\b|\\bSYC\\b|\\bSLE\\b|\\bSGP\\b|\\bSVK\\b|\\bSVN\\b|\\bSLB\\b|\\bSOM\\b|\\bZAF\\b|\\bSGS\\b|\\bESP\\b|\\bLKA\\b|\\bSDN\\b|\\bSUR\\b|\\bSJM\\b|\\bSWZ\\b|\\bSWE\\b|\\bCHE\\b|\\bSYR\\b|\\bTWN\\b|\\bTWN\\b|\\bTJK\\b|\\bTZA\\b|\\bTHA\\b|\\bTLS\\b|\\bTGO\\b|\\bTKL\\b|\\bTON\\b|\\bTTO\\b|\\bTTO\\b|\\bTUN\\b|\\bTUR\\b|\\bTKM\\b|\\bTCA\\b|\\bTUV\\b|\\bUGA\\b|\\bUKR\\b|\\bARE\\b|\\bGBR\\b|\\bUSA\\b|\\bUMI\\b|\\bURY\\b|\\bUZB\\b|\\bVUT\\b|\\bVEN\\b|\\bVEN\\b|\\bVNM\\b|\\bVNM\\b|\\bVGB\\b|\\bVIR\\b|\\bWLF\\b|\\bESH\\b|\\bYEM\\b|\\bZMB\\b|\\bZWE\\b"""

        address = df.Address.tolist()
        remarks = df.Remarks.tolist()
        dob = df.DOB.tolist()
        nationality = df.Nationality.tolist()

        print(len(address))
        print(len(remarks))
        print(len(dob))

        pakistani = []

        for i in range(len(address)):

            try:
                dob_ = dob[i]
                x = json.loads(dob_)['POB']
                y = []
                try:
                    if len(x) > 0:
                        if len(re.findall(cities_re, x, flags = re.IGNORECASE)) > 0:
                            pakistani.append(1)
                            continue
                except:
                    pass
            except:
                pass

            try:
                add = address[i]
                add = json.loads(add)['address info']
                y = []
                try:
                    if len(x) > 0:
                        for j in x:
                            y.append(j['country'])
                except:
                    pass
                if len(y) > 0:
                    con = " ".join(y)
                    if len(re.findall(country_code, con, flags = re.IGNORECASE)) > 0 and len(re.findall(r"pakistan|\bpk\b", con, flags = re.IGNORECASE)) == 0:
                        pakistani.append(0)
                        continue
                    elif len(re.findall(r"pakistan|\bpk\b", con, flags = re.IGNORECASE)) > 0:
                        pakistani.append(1)
                        continue
            except:
                pass    

            try:
                rem = remarks[i]
                if len(re.findall(cities_re, rem, flags = re.IGNORECASE)) > 0:
                    if len(re.findall(r"india", rem, flags = re.IGNORECASE)) > 0 and len(re.findall(r"pakistan|\bpk\b", rem, flags = re.IGNORECASE)) == 0:
                        pakistani.append(0)
                        continue
                    pakistani.append(1)
                    continue
            except:
                pass    

            try:
                x = re.findall(cities_re, address[i], flags = re.IGNORECASE)
                if len(x) > 0:
                    pakistani.append(1)
                    continue
            except:
                pass

            pakistani.append(0)

        pakistani = [True if i == 1 else False for i in pakistani]
        df = df.loc[pakistani]

        df = df.reset_index(drop = True)

        return df         


