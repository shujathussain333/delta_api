#!/usr/bin/env bash

cd delta_api/
python manage.py collectstatic --noinput
python manage.py runserver 0.0.0.0:8000
