"""restful_auth URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include

from api.views.auth_views import endpoint_disabled, ResetForgottenPassword
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_swagger.views import get_swagger_view
from api.views import views

schema_view = get_swagger_view(title='Delta API')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('',schema_view),

    # Djoser endpoints that have been explicitly disabled
    path('auth/users/create/', endpoint_disabled),
    path('auth/users/delete/', endpoint_disabled),
    path('auth/users/activate/', endpoint_disabled),
    path('auth/users/email/', endpoint_disabled),

    # Djoser endpoints
    re_path(r'auth/reset_password/(?P<uid>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            ResetForgottenPassword.as_view()),
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.jwt')),
    path('logout',views.LogoutView.as_view(), name='logged_out'),

    # API endpoints
    path('api/', include('api.urls'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
